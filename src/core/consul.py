from up_consul import ConsulClient


class FlaskConsul:

    def __init__(self, app=None):
        self.client = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.client = ConsulClient(
            host=app.config.get('CONSUL_HOST'),
            token=app.config.get('CONSUL_TOKEN'),
        )

    def is_active(self, service_code, setting_type, service_type='metrics'):
        return self.client.kv.get_value(
            f'{service_type}/{service_code}/{setting_type}/active'
        )

    @staticmethod
    def ordered_items(dict_obj):
        for key, value in sorted(
                dict_obj.items(),
                key=lambda x: x[1].get('order') or 999
        ):
            yield key, value

    def get_settings(self, service_code, setting_type, setting_base='metrics'):
        """Fetch metrics settings for a given service.

        Args:
            service_code (str): Identifying metrics service in Central Station.
            setting_type (str): Type of metrics setting, based on the plugin.
            setting_base (str): Adjust for presses, journals, etc, settings.

        Returns:
            tuple (dict, bool): Metrics settings and if the settings are active.
        """
        key_base = f'{setting_base}/{service_code}/{setting_type}'
        settings = self.client.kv.get_many(key_base)

        settings_tree = self.client.kv.compile_service_settings(
            key_base,
            settings,
         )
        is_active = settings_tree.pop('active', None)

        return settings_tree, is_active
