import logging

from core import db


logger = logging.getLogger(__name__)


def get_or_create(model, **filter_parameters):
    instance = model.query.filter_by(**filter_parameters).first()
    if not instance:
        instance = model(**filter_parameters)
        db.session.add(instance)
        db.session.commit()

    return instance


def get_enum_by_value(enum_class, value):
    """ Fetch enum object by its integer value.

    Args:
        enum_class (enum.EnumMeta): Any defined Enum class
        value (int): Integer value of the enum

    Returns:
        enum.Enum: enum object or None
    """
    try:
        return enum_class(value)
    except ValueError:
        return None


def get_enum_by_name(enum_class, name):
    """  Fetch enum object by its name.

    Args:
        enum_class (enum.EnumMeta): Any defined Enum class
        name (str): name of the enum object

    Returns:
        Enum: enum object or None
    """
    try:
        return enum_class[name]
    except KeyError:
        return None
