"""
Common settings for the metrics-drivers-wrapper project.
"""

from enum import IntEnum
from os import getenv, pardir, path

from generic import utils


class StaticProviders(IntEnum):
    """Used to keep track of the provider of a given event. Used by Plugins
    to provide a link to the full event record, based on the external event ID.
    """
    crossref_cited_by = 1
    google_analytics = 2
    access_logs = 3
    access_logs_local = 4
    jstor = 5
    google_books = 6
    matomo = 7
    irus_uk = 8
    unglue_it = 9
    figshare = 10


class Config:

    SERVICE_VERSION = '0.2.86'

    SECRET_KEY = getenv('SECRET_KEY', 'secret-key')

    APP_DIR = path.dirname(path.dirname(path.abspath(__file__)))
    PROJECT_ROOT = path.abspath(path.join(APP_DIR, pardir))

    DRIVERS_SETTINGS_SOURCE = getenv('DRIVERS_SETTINGS_SOURCE', 'CONSUL')

    # ## DATABASES ##

    DB_USER = getenv('DB_USER')
    DB_PASSWORD = getenv('DB_PASSWORD')
    DB_HOST = getenv('DB_HOST')
    DB_NAME = getenv('DB_NAME')

    DB_BACKEND = getenv('DB_BACKEND', 'postgresql+psycopg')
    DB_PORT = getenv('DB_PORT', '5432')

    SQLALCHEMY_DATABASE_URI = (
        '{backend}://{user}:{password}@{host}:{port}/{db}'
    ).format(
        backend=DB_BACKEND,
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT,
        db=DB_NAME,
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # ## Tokens API endpoint and authentication ##

    TOKENS_KEY = getenv('TOKENS_KEY')
    TOKENS_EMAIL = getenv('TOKENS_EMAIL', 'tech@example.com')

    ALTMETRICS_USER = getenv('ALTMETRICS_USER', 'tech@example.com')
    ALTMETRICS_PASSWORD = getenv('ALTMETRICS_PASSWORD', 'tech_pass')

    TRANSLATION_API_BASE = getenv('TRANSLATION_API_BASE')

    # REDIS

    REDIS_HOST = getenv('REDIS_HOST', '127.0.0.1')
    REDIS_PORT = int(getenv('REDIS_PORT', 6379))

    # ## CELERY ##

    RMQ_AMQ_SCHEME = getenv('RMQ_AMQ_SCHEME', 'amqps')
    RMQ_USER = getenv('RMQ_USER')
    RMQ_PASSWORD = getenv('RMQ_PASSWORD')
    RMQ_HOST = getenv('RMQ_HOST')
    RMQ_PORT = getenv('RMQ_PORT', '5672')
    RMQ_VHOST = getenv('RMQ_VHOST')

    AMQ_URL = '{amq_scheme}://{user}:{password}@{host}:{port}/{vhost}'
    RESULT_BACKEND = None
    CELERY_BROKER_URL = AMQ_URL.format(
        amq_scheme=RMQ_AMQ_SCHEME,
        user=RMQ_USER,
        password=RMQ_PASSWORD,
        host=RMQ_HOST,
        port=RMQ_PORT,
        vhost=RMQ_VHOST
    )

    # ## CONSUL ##

    CONSUL_HOST = getenv('CONSUL_HOST')
    CONSUL_TOKEN = getenv('CONSUL_TOKEN')

    # ## PLUGINS ##
    #
    PLUGINS = utils.load_plugins(
        folder='plugins',
        ignore=[
            'logic.py',
            'generic',
            '__init__.py',
            '__pycache__',
        ]
    )

    # ## SENTRY ##

    SENTRY_DSN = getenv('SENTRY_DSN')

    # ## Ignore settings for testing: ##
    if getenv('CONFIG') != 'TestConfig':
        # ## METRICS_API ##

        METRICS_API_BASE = getenv('METRICS_API_BASE', 'http://localhost:8080')


class DevConfig(Config):
    DEBUG = True
    FLASK_ENV = 'development'


class LiveConfig(Config):
    DEBUG = False
    FLASK_ENV = 'production'


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    FLASK_ENV = 'testing'
    DRIVERS_SETTINGS_SOURCE = 'YAML'
