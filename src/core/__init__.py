# -*- coding: utf-8 -*-
import os

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from flask_api import FlaskAPI
from flask_migrate import Migrate

from .celery import FlaskCelery
from .consul import FlaskConsul
from .database import db
from .plugins import MetricsPlugins


CONFIG = os.getenv('CONFIG', 'DevConfig')
consul = FlaskConsul()
plugins = MetricsPlugins()
celery_app = FlaskCelery()


def create_app():
    app = FlaskAPI(__name__, template_folder='templates')
    app.config.from_object(f'core.settings.{CONFIG}')

    if app.config.get('SENTRY_DSN'):
        sentry_sdk.init(
            dsn=app.config.get('SENTRY_DSN'),
            release=app.config.get('SERVICE_VERSION'),
            environment=os.getenv('SENTRY_ENV', 'production'),
            integrations=[FlaskIntegration()]
        )

    from processor.tasks import process_plugin  # noqa - required by celery

    plugins.init_app(app)
    db.init_app(app)
    Migrate(app, db)

    if CONFIG != 'TestConfig':
        consul.init_app(app)
        celery_app.init_app(app, plugins)

    return app


if __name__ == "__main__":
    application = create_app()
    application.run()
