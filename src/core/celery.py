from celery import Celery
from celery.schedules import crontab


class FlaskCelery(Celery):

    celery = None

    def __init__(self, app=None, plugins=None, *args, **kwargs):

        super().__init__(*args, **kwargs)

        if app and plugins:
            self.init_app(app, plugins)

    def init_app(self, app, plugins):
        """Instantiate celery in the main application."""

        self.__init__(
            main=app.import_name,
            backend=app.config['RESULT_BACKEND'],
            broker=app.config['CELERY_BROKER_URL']
        )

        class ContextTask(self.Task):

            def __call__(self, *args, **kwargs):
                with app.app_context():
                    return super().__call__(*args, **kwargs)

        self.Task = ContextTask
        self.conf.update(app.config)
        self.configure_celery(plugins)

    def configure_celery(self, plugins):
        """Add configuration for celery tasks, including per-plugin routes."""

        self.conf.beat_schedule = {
            'create-scrapes-every-day': {
                'task': 'create-scrapes',
                'schedule': crontab(minute=30, hour=9, day_of_week='*')
            },
            'pull-metrics-every-day': {
                'task': 'pull-metrics',
                'schedule': crontab(minute=30, hour=10, day_of_week='*')
            },
            'send-metrics-every-day': {
                'task': 'send-metrics',
                'schedule': crontab(minute=0, hour=6, day_of_week='*')
            },
        }

        celery_task_routes = {
            'create-scrapes': {'queue': 'metrics-service.create-scrapes'},
            'pull-metrics': {'queue': 'metrics-service.pull-metrics'},
            'send-metrics': {'queue': 'metrics-service.send-metrics'},
        }

        for plugin_task in plugins.plugin_task_names:
            celery_task_routes.update(
                {plugin_task: {'queue': f'metrics-service.{plugin_task}'}}
            )

        self.conf.task_routes = celery_task_routes
