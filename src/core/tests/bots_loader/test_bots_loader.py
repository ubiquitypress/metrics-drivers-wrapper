import json
import os
import re
import tempfile
import unittest

from .demo_agents import user_agents
from bots_loader.bots_loader import BotPatternsUpdater, BotsDetector


class TestBotPatternsUpdater(unittest.TestCase):
    def setUp(self) -> None:
        # Create a temporary file for testing
        self.temp_file = tempfile.NamedTemporaryFile(mode='w', delete=False)
        self.output_file_path = self.temp_file.name
        self.user_agents = "UserAgent1\nUserAgent2\nUserAgent3"
        self.common_bot_words = [
            "bot",
            "crawler",
            "spider",
            "googlebot",
            "check",
            "scan",
            "surf",
            "robot",
            "cobitsprobe",
            "codegator",
        ]
        self.robots_list = []
        self.bot_patterns_updater = BotPatternsUpdater(
            user_agents=self.user_agents,
            robots_list=self.robots_list,
            output_file=self.output_file_path,
            common_bot_words=self.common_bot_words
        )

    def tearDown(self):
        # Close and remove the temporary file
        self.temp_file.close()
        os.remove(self.output_file_path)

    def create_temporary_file(self, content: str) -> None:
        """
        Create a temporary file with the specified content and return its path
        """
        temp_file = tempfile.NamedTemporaryFile(mode="w+", delete=False)
        temp_file.write(content)
        temp_file.close()
        return temp_file.name

    def test_generate_keywords_from_spiders_integration(self):
        """
        Test case with example content in the spiders file
        Will make the results lowercase and delete duplicates
        """
        sample_spiders_content = """
        User-Agent: Bot-1234567890
        User-Agent: Crawler-1234567890
        User-Agent: Spider-1234567890
        User-Agent: Googlebot-1234567890
        User-Agent: CheckingBot-1234567890
        User-Agent: ScanningBot-1234567890
        User-Agent: SurfingBot-1234567890
        User-Agent: RobotBot-1234567890
        User-Agent: CobitsProbe-1234567890
        User-Agent: CodeGator-1234567890
        """
        expected_output = [
            "bot",
            "check",
            "cobitsprobe",
            "codegator",
            "crawler",
            "googlebot",
            "robot",
            "scan",
            "spider",
            "surf"
        ]

        self.bot_patterns_updater.user_agents = sample_spiders_content
        result = self.bot_patterns_updater.generate_keywords_from_spiders()
        self.assertIsInstance(result, list)
        self.assertEqual(result, expected_output)

    def test_generate_keywords_from_spiders_no_matching_lines(self):
        """Test case with lines that do not match common_bot_words"""
        example_spiders_content = """
        User-Agent: FriendlyBot
        User-Agent: HelpfulCrawler
        """
        expected_output = ["bot", "crawler"]

        self.bot_patterns_updater.user_agents = example_spiders_content
        result = self.bot_patterns_updater.generate_keywords_from_spiders()
        self.assertIsInstance(result, list)
        self.assertEqual(result, expected_output)

    def test_simplify_keywords(self) -> None:
        """Load bots keywords and simplify them if they are too long."""
        keywords = [
            "bot",
            "crawler",
            "Irrelevant http://example.com More than 20 characters",
            "short",
            "12234bot5678",
        ]
        common_bot_words = ["bot", "crawler"]
        pattern_http = re.compile(r"https?://([^/ ]+)")
        simplified_keywords = self.bot_patterns_updater.simplify_keywords(
            keywords, common_bot_words, pattern_http
        )
        self.assertTrue(simplified_keywords)
        self.assertIsInstance(simplified_keywords, list)
        self.assertEqual(
            sorted(simplified_keywords),
            ["bot", "crawler", "example.com", "short"],
        )

    def test_sanitize_lines_spiders(self) -> None:
        """Delete slashes when reading the spiders.txt file"""
        user_agents = "Line 1\nLine 2\nLine with \\ slashes\nAnother line\\"
        expected_output = [
            "Another line", "Line 1", "Line 2", "Line with  slashes"
        ]
        sanitized_keywords = self.bot_patterns_updater.sanitize_lines_spiders(
            user_agents
        )
        self.assertTrue(sanitized_keywords)
        self.assertIsInstance(sanitized_keywords, list)
        self.assertEqual(
            sorted(sanitized_keywords), expected_output
        )

    def test_sanitize_lines_spiders_empty_input(self) -> None:
        """Test case with an empty input"""
        input_user_agents = ""
        expected_output = []

        result = self.bot_patterns_updater.sanitize_lines_spiders(
            input_user_agents
        )
        self.assertIsInstance(result, list)
        self.assertEqual(result, expected_output)

    def test_sanitize_lines_spiders_single_line(self) -> None:
        """Test case with a single line without slashes"""
        input_user_agents = "Single Line without slashes"
        expected_output = ["Single Line without slashes"]

        result = self.bot_patterns_updater.sanitize_lines_spiders(
            input_user_agents
        )
        self.assertIsInstance(result, list)
        self.assertEqual(result, expected_output)

    def test_update_bot_patterns(self) -> None:
        initial_robots_list = json.loads(
            json.dumps(self.bot_patterns_updater.robots_list)
        )
        self.bot_patterns_updater.update_bot_patterns()
        self.assertNotEqual(
            self.bot_patterns_updater.robots_list, initial_robots_list
        )


class TestBotsDetector(unittest.TestCase):
    def setUp(self) -> None:
        self.detector = BotsDetector()

    def test_is_bot_true(self) -> None:
        """Test with user agents that matches a pattern in robots_list"""
        user_agent = (
            "Mozilla/5.0 (compatible; Googlebot/2.1; "
            "+http://www.google.com/bot.html)"
        )
        self.assertTrue(self.detector.is_bot(user_agent))

        user_agent = "Googlebot/2.1 (+http://www.google.com/bot.html)"
        self.assertTrue(self.detector.is_bot(user_agent))

        user_agent = (
            "Mozilla/5.0 (compatible; YandexRenderResourcesBot/1.0; "
            "+http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/108.0.0.0"
        )
        self.assertTrue(self.detector.is_bot(user_agent))

    def test_is_bot_false(self) -> None:
        """
        Test with user agents that does not match any pattern in robots_list
        """
        for user_agent in user_agents:
            self.assertFalse(self.detector.is_bot(user_agent))
