chrome_windows = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
chrome_mac = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
chrome_android = "Mozilla/5.0 (Linux; Android 11; SM-G991U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Mobile Safari/537.36"
chrome_ios = "Mozilla/5.0 (iPhone; CPU iPhone OS 14_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/93.0.4577.82 Mobile/15E148 Safari/604.1"
chrome_ubuntu = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36"
chrome_fedora = "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
chrome_debian = "Mozilla/5.0 (X11; Debian; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36"
chrome_arch = "Mozilla/5.0 (X11; Linux x86_64; rv:92.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"

firefox_windows = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0"
firefox_mac = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:92.0) Gecko/20100101 Firefox/92.0"
firefox_android = "Mozilla/5.0 (Android 11; Mobile; rv:92.0) Gecko/92.0 Firefox/92.0"
firefox_ios = "Mozilla/5.0 (iPhone; CPU iPhone OS 14_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) FxiOS/38.0 Mobile/15E148 Safari/605.1.15"
firefox_ubuntu = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0"
firefox_fedora = "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0"
firefox_debian = "Mozilla/5.0 (X11; Debian; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
firefox_arch = "Mozilla/5.0 (X11; Arch Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0"

safari_mac = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15"
safari_ios = "Mozilla/5.0 (iPhone; CPU iPhone OS 14_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Mobile/15E148 Safari/604.1"

edge_windows = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Edg/93.0.961.47"
edge_mac = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Edg/93.0.961.47"
edge_ubuntu = "Mozilla/5.0 (X11; Linux x86_64; Ubuntu; rv:92.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Edg/93.0.961.38"
edge_fedora = "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:92.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Edg/93.0.961.38"

opera_windows = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 OPR/79.0.4143.72"
opera_mac = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 OPR/79.0.4143.72"
opera_ubuntu = "Mozilla/5.0 (X11; Linux x86_64; Ubuntu; rv:89.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 OPR/76.0.4017.177"
opera_fedora = "Mozilla/5.0 (X11; Fedora Linux; Linux x86_64; rv:89.0) AppleWebKit/537.36 (KHTML, like Gecko) OPR/77.0.4054.277"

brave_windows = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Brave/93.0.4577.82"
brave_mac = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Brave/93.0.4577.82"
brave_ubuntu = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36 Brave/93.0.4577.82"
brave_fedora = "Mozilla/5.0 (X11; Fedora Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Brave/89.0.4389.114"


user_agents = [
    chrome_windows,
    chrome_mac,
    chrome_android,
    chrome_ios,
    chrome_ubuntu,
    chrome_fedora,
    chrome_debian,
    chrome_arch,
    firefox_windows,
    firefox_mac,
    firefox_android,
    firefox_ios,
    firefox_ubuntu,
    firefox_fedora,
    firefox_debian,
    firefox_arch,
    safari_mac,
    safari_ios,
    edge_windows,
    edge_mac,
    edge_ubuntu,
    edge_fedora,
    opera_windows,
    opera_mac,
    opera_ubuntu,
    opera_fedora,
    brave_windows,
    brave_mac,
    brave_ubuntu,
    brave_fedora,
]
