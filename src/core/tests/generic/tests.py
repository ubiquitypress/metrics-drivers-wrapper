import unittest

from core import create_app, plugins


class PluginTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def test_load_yaml_settings(self):
        """Test yaml settings load correctly with 'load_yaml_settings' function."""
        access_logs_driver = plugins.get_plugin(3).PROVIDER
        settings, is_active = access_logs_driver.load_yaml_settings('demo')
        self.assertEqual(
            settings['json_key']['project_id'],
            'test-accesslogs',
        )
