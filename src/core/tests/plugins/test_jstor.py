import os
import tempfile
import unittest
from unittest.mock import MagicMock

import pandas as pd

from core import create_app, db, plugins
from core.settings import StaticProviders
from plugins.jstor.logic import (
    standardise_uri, load_data_from_file
)


class TestJstorLogic(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        current_file_path = os.path.abspath(__file__)
        self.file_path = os.path.dirname(current_file_path)
        # Simplified data with 7 rows of irrelevant data
        irrelevant_data = [['']*10]*7  # 7 rows of irrelevant data
        relevant_data_file_1_2023 = {
            'Country Name': ['United States', 'Finland', 'United Kingdom'],
            'Book Title': ['Physics for Scientists and Engineers', 'Machine Learning', 'Artificial Intelligence'],
            'Book ID': ['10.2307/j.ctvggx2cr12', '10.2307/j.ctvggx2cr13', '10.2307/j.ctvggx2cr14'],
            'Authors': ['Paul A. Tipler, Gene Mosca', 'Stephen Marsland', 'Nils J. Nilsson'],
            'ISBN': ['978-1-942490-34-2', '978-1-4200-6608-9', '978-1-55860-467-4'],
            'Feb-2023': ['32', '3', '66'],
            'Mar-2023': ['42', '70', '25'],
            'Apr-2023': ['98', '52', '97'],
        }

        relevant_data_file_2_2024 = {
            'Country Name': ['United States', 'Finland', 'United Kingdom'],
            'Book Title': ['Physics for Scientists and Engineers', 'Machine Learning', 'Artificial Intelligence'],
            'Book ID': ['10.2307/j.ctvggx2cr12', '10.2307/j.ctvggx2cr13', '10.2307/j.ctvggx2cr14'],
            'Authors': ['Paul A. Tipler, Gene Mosca', 'Stephen Marsland', 'Nils J. Nilsson'],
            'ISBN': ['978-1-942490-34-2', '978-1-4200-6608-9', '978-1-55860-467-4'],
            'Feb-2024': ['8', '9', '10'],
            'Mar-2024': ['11', '22', '33'],
            'Apr-2024': ['101', '105', '110'],
        }

        # Create a temporary directory and file
        self.test_dir = tempfile.TemporaryDirectory()
        self.service_code = 'test_service'
        self.log_dir = os.path.join(self.test_dir.name, self.service_code)
        os.makedirs(self.log_dir)

        # Original file with original relevant data
        self.file_path_original = os.path.join(self.log_dir, 'test_original.xlsx')
        with pd.ExcelWriter(self.file_path_original, engine='openpyxl') as writer:
            pd.DataFrame(irrelevant_data).to_excel(writer, index=False, header=False)
            pd.DataFrame(relevant_data_file_1_2023).to_excel(writer, startrow=8, index=False, header=True)

        # Modified file with modified relevant data
        self.file_path_modified = os.path.join(self.log_dir, 'test_modified.xlsx')
        with pd.ExcelWriter(self.file_path_modified, engine='openpyxl') as writer:
            pd.DataFrame(irrelevant_data).to_excel(writer, index=False, header=False)
            pd.DataFrame(relevant_data_file_2_2024).to_excel(writer, startrow=8, index=False, header=True)

    def tearDown(self):
        self.test_dir.cleanup()  # Cleanup the temporary directory
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_standardise_uri(self):
        self.assertEqual(
            standardise_uri('1234567890'),
            'urn:isbn:1234567890'
        )

    def test_load_data_from_file_1(self):

        search_month = 'Mar-2023'
        result_df = load_data_from_file(search_month, self.log_dir)

        self.assertIsNotNone(result_df)
        self.assertIn(search_month, result_df.columns)
        self.assertNotIn('Mar-2024', result_df.columns)

    def test_load_data_from_file_2(self):

        search_month = 'Mar-2024'
        result_df = load_data_from_file(search_month, self.log_dir)

        self.assertIsNotNone(result_df)
        self.assertIn(search_month, result_df.columns)
        self.assertNotIn('Mar-2023', result_df.columns)

    def test_load_data_from_file_wrong_file_name(self):
        search_month = 'Sep-2023'
        log_dir = f'{self.file_path}/jstor_files/test'

        with self.assertRaises(FileNotFoundError):
            load_data_from_file(search_month, log_dir)

    def test_load_data_from_file_wrong_date(self):
        search_month = 'Sep-2018'
        log_dir = f'{self.file_path}/jstor_files/test'

        with self.assertRaises(FileNotFoundError):
            load_data_from_file(search_month, log_dir)

    def test_load_data_from_file_wrong_date(self):
        search_month = 'Sep-2018'
        log_dir = f'{self.file_path}/jstor_files/test'

        with self.assertRaises(FileNotFoundError):
            load_data_from_file(search_month, log_dir)

    def test_running_plugin_gets_correct_values(self):
        """Test that jstor plugin extracts correct values for a given date."""
        service_code = 'demo'
        jstor_plugin = plugins.get_plugin(StaticProviders.jstor).PROVIDER
        jstor_plugin.multi_scheme_uri_to_id = MagicMock(
            return_value=[{'URI': 'work_1'}]
        )
        events = jstor_plugin.process(
            search_date='2023-11-30',
            service_code=service_code,
            enforce_active=False,
        )
        events = list(events)
        self.assertEqual(len(events), 4)
        events_total = sum(event.value for event in events)
        self.assertEqual(events_total, 55)

    def test_running_plugin_ignores_zero_fields(self):
        """Test that jstor plugin does not metrics if the value is 0."""
        service_code = 'demo'
        jstor_plugin = plugins.get_plugin(StaticProviders.jstor).PROVIDER
        jstor_plugin.multi_scheme_uri_to_id = MagicMock(
            return_value=[{'URI': 'work_1'}]
        )
        events = jstor_plugin.process(
            search_date='2023-10-31',
            service_code=service_code,
            enforce_active=False,
        )
        events = list(events)
        self.assertEqual(len(events), 1)
        self.assertEqual(events[0].value, 1)
