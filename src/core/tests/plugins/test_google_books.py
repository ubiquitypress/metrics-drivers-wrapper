import os
import tempfile
import unittest
from unittest.mock import MagicMock

from core import create_app, db, plugins
from core.settings import StaticProviders
from generic.mount_point import DriverNotReadyException
from plugins.google_books.logic import load_data_from_file


class TestGoogleBooks(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        self.temp_dir = tempfile.TemporaryDirectory()  # Create a temp directory
        self.service_code = 'test_service'
        self.log_dir = os.path.join(self.temp_dir.name, self.service_code)
        os.makedirs(self.log_dir)

        self.data = """
        Primary ISBN,Title,Book Visits (BV),BV with Pages Viewed,Non-Unique Buy Clicks,BV with Buy Clicks,Buy Link CTR,Pages Viewed
        9781909188082,Conversations with Kenelm,80,69,0,0,0.0%,216
        9781909188099,Strindberg and Autobiography,42,37,0,0,0.0%,93
        9781909188105,Studies in Strindberg,39,35,0,0,0.0%,300
        9781909188112,Dante and Aquinas,84,78,0,0,0.0%,368
        """

        self.file_name = f'GoogleBooksTrafficReport-2023-06-to-2023-06.csv'
        with open(os.path.join(self.log_dir, self.file_name), 'w', encoding="utf-16") as file:
            file.write(self.data)

    def tearDown(self):
        self.temp_dir.cleanup()  # Cleanup the temporary directory
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_load_data_from_file(self):
        result = load_data_from_file(
            "2023-06-31", self.log_dir
        )
        self.assertEqual(result, self.data)

    def test_load_data_from_file_wrong_log_dir(self):
        with self.assertRaises(FileNotFoundError):
            load_data_from_file("2023-06-09", "not/self.log_dir")

    def test_load_data_from_file_wrong_date(self):
        result = load_data_from_file("2024-10-10", self.log_dir)
        self.assertEqual(result, None)

        result = load_data_from_file("not a date", self.log_dir)
        self.assertEqual(result, None)

    def test_running_plugin_extracts_expected_values(self):
        """Test that Google Books plugin Extracts the expected total metrics values from a file."""
        service_code = 'demo'
        gbooks_plugin = plugins.get_plugin(StaticProviders.google_books).PROVIDER
        gbooks_plugin.multi_scheme_uri_to_id = MagicMock(
            return_value=[{'URI': f'info:doi:10.12345/book-3'}]
        )
        events = gbooks_plugin.process(
            search_date='2023-10-31',
            service_code=service_code,
            enforce_active=False,
        )
        events = list(events)
        self.assertEqual(events[0].value, 1585)

    def test_running_plugin_raises_correct_exception_when_file_not_ready(self):
        """Test that google books plugin raises the correct excpetion when a required file is not found."""
        service_code = 'demo'
        gbooks_plugin = plugins.get_plugin(StaticProviders.google_books).PROVIDER
        gbooks_plugin.multi_scheme_uri_to_id = MagicMock(
            return_value=[{'URI': f'info:doi:10.12345/book-3'}]
        )
        events = gbooks_plugin.process(
            search_date='2023-01-31',
            service_code=service_code,
            enforce_active=False,
        )
        with self.assertRaises(DriverNotReadyException):
            list(events)


if __name__ == '__main__':
    unittest.main()
