import re
import unittest

from plugins.access_logs.logic import calculate_request_hosts, remove_submatch


class AccessLogsDriverTestCase(unittest.TestCase):

    def test_access_logs_calculate_request_hosts(self):
        """Test calculate_request_hosts function returns
        the correct values given a range of inputs."""
        test_host_filters = [
            '"press.demo.edu" OR "www.press.demo.edu"',
            '"press.demo.edu"',
            ' "www.press.demo.edu" OR "press.demo.edu "',
            ' "www.press.demo.edu" ',
        ]
        results = [
            calculate_request_hosts(host_filters)
            for host_filters in test_host_filters
        ]
        expected_results = [
            ['press.demo.edu', 'www.press.demo.edu'],
            ['press.demo.edu'],
            ['www.press.demo.edu', 'press.demo.edu'],
            ['www.press.demo.edu']
        ]
        self.assertEqual(
            results,
            expected_results,
            'Access Logs - Incorrect request hosts calculated.',
        )

    def test_access_logs_remove_submatch(self):
        """Test remove_submatch function removes sub-match correctly."""
        test_paths = [
            "/work/123/download/992fa2210s021.pdf",
            "/en/work/123/download/992fa2210s021.pdf",
            "/es/work/123/download/992fa2210s021.pdf",
            "/fr/work/123/download/992fa2210s021.pdf",
        ]
        pattern = re.compile(r'/(?P<remove>\w{2}/)?work/\d+/download/[\w\d-]+\.\w{3}')
        results = set(
            remove_submatch(pattern, path, 'remove') for path in test_paths
        )
        expected_results = {"/work/123/download/992fa2210s021.pdf"}

        self.assertEqual(
            results,
            expected_results,
            'Access Logs - Incorrect remove_submatch path returned.',
        )

    def test_access_logs_remove_submatch_doesnt_fail_without_group_in_regex(self):
        """Test remove_submatch function still works when there is no group in the regex."""
        test_paths = [
            "/work/123/download/992fa2210s021.pdf",
            "/en/work/123/download/992fa2210s021.pdf",
            "/es/work/123/download/992fa2210s021.pdf",
            "/fr/work/123/download/992fa2210s021.pdf",
        ]
        pattern = re.compile(r"/work/\d+/download/[\w\d]+[.]\w{3}")
        results = set(
            remove_submatch(pattern, path, 'remove') for path in test_paths
        )
        expected_results = {'', "/work/123/download/992fa2210s021.pdf"}

        self.assertEqual(
            results,
            expected_results,
            'Access Logs - Incorrect remove_submatch path returned (2).',
        )

    def test_access_logs_remove_submatch_works_when_group_not_found(self):
        """Test remove_submatch function still works when group is not found."""
        test_paths = [
            "/work/123/download/992fa2210s021.pdf",
            "/en/work/123/download/992fa2210s021.pdf",
            "/es/work/123/download/992fa2210s021.pdf",
            "/fr/work/123/download/992fa2210s021.pdf",
        ]
        pattern = re.compile(r'/(?P<keep>\w{2}/)?work/\d+/download/[\w\d-]+\.\w{3}')
        results = set(
            remove_submatch(pattern, path, 'remove') for path in test_paths
        )
        expected_results = set(test_paths)

        self.assertEqual(
            results,
            expected_results,
            'Access Logs - Incorrect remove_submatch path returned (3).',
        )
