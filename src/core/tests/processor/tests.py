from os import environ
import unittest
from unittest.mock import patch

from sqlalchemy.exc import IntegrityError

from core import create_app, db
from processor.tasks import process_plugin, create_scrapes

from .variables import (
    event_1__all_fields_present,
    event_2__all_fields_present,
    event_1_duplicate_with_both_event_uri_and_country_is_null,
    event_2_duplicate_with_both_event_uri_and_country_is_null,
    event_1_duplicate_with_country_is_null,
    event_2_duplicate_with_country_is_null,
    event_1_duplicate_with_different_value,
    event_3_country_is_null,
    event_3_duplicate_different_value,
    event_4_country_is_null_event_uri_is_null,
    event_4_duplicate_different_value,
    scrape_1,
    scrape_2,
    scrape_3,
)

environ['CONFIG'] = 'TestConfig'


valid_event_set = [
    event_1__all_fields_present,
    event_2__all_fields_present,
    event_1_duplicate_with_both_event_uri_and_country_is_null,
    event_2_duplicate_with_both_event_uri_and_country_is_null,
    event_1_duplicate_with_country_is_null,
    event_2_duplicate_with_country_is_null,
    event_3_country_is_null,
    event_4_country_is_null_event_uri_is_null,
]
invalid_duplicates = [
    event_1_duplicate_with_different_value,
    event_3_duplicate_different_value,
    event_4_duplicate_different_value,
]

invalid_event_set = valid_event_set + invalid_duplicates


def create_event_copies():
    from processor.models import Event

    valid_event_copy = []
    for event in valid_event_set:
        event_dict = event.__dict__.copy()
        event_dict.pop('_sa_instance_state')
        valid_event_copy.append(Event(**event_dict))

    invalid_duplicates_copy = []
    for event in invalid_duplicates:
        event_dict = event.__dict__.copy()
        event_dict.pop('_sa_instance_state')
        invalid_duplicates_copy.append(Event(**event_dict))

    return valid_event_copy, invalid_duplicates_copy


class ModelIntegrityTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def _assert_valid_events(self, *events):
        """Save new entries to the database. Assert that the correct number
        of entries are created.

        Args:
            *events (model): Instances to add to the database.
        """
        for entry in events:
            db.session.add(entry)
        db.session.commit()

        self.assertEqual(self._count_events(), len(events))

    def _assert_invalid_entries(self, *entries):
        """Assert that an exception is raised when adding invalid entries to
        the database.

        Args:
            *entries (model): Instances to add to the database.
        """
        for entry in entries:
            db.session.add(entry)

        with self.assertRaises(IntegrityError):
            db.session.commit()

    def _count_events(self):
        from processor.models import Event
        return Event.query.count()

    def _count_scrapes(self):
        from processor.models import Scrape
        return Scrape.query.count()

    # ## Check dodgy Scrapes are deleted

    @patch('generic.mount_point.consul')
    @patch('processor.tasks.consul')
    def test_create_scrapes_does_not_crash_when_driver_is_not_configured(self, generic_consul, tasks_consul):
        """Ensure inconfigured drivers do not raise exceptions when creating scrapes."""
        self.app.config['DRIVERS_SETTINGS_SOURCE'] = 'CONSUL'
        tasks_consul.client.kv.get_many.return_value = {'xxx/code-1/yyy': None}
        generic_consul.get_settings.return_value = ({}, None)
        create_scrapes()
        self.assertEqual(self._count_scrapes(), 0)

    @patch('generic.mount_point.consul')
    def test_process_plugin_handles_unconfigured_drivers_gracefully(self, consul_client):
        """Ensure invalid scrapes do not raise exceptions, and are deleted."""
        self.app.config['DRIVERS_SETTINGS_SOURCE'] = 'CONSUL'
        scrapes = [scrape_1, scrape_2, scrape_3]

        for scrape in scrapes:
            db.session.add(scrape)
        db.session.commit()

        consul_client.get_settings.return_value = {}, False
        for scrape in scrapes:
            process_plugin(scrape.id)

        self.assertEqual(self._count_scrapes(), 0)

    #  ## Create all entries to avoid integrity errors down the line

    def test_event_is_valid_works_correctly(self):
        """Assert that existing entries are ignored when event_is_valid is
        called.
        """
        invalid_duplicate_copies, valid_event_copies = create_event_copies()
        for event in invalid_duplicate_copies:
            db.session.add(event)

        db.session.commit()

        for event in valid_event_copies:
            if event.is_unique():
                db.session.add(event)

        db.session.commit()

        self.assertEqual(self._count_events(), len(valid_event_set))

    def test_creating_all_entries_throws_exception(self):
        self._assert_invalid_entries(*invalid_event_set)

    #  ## Test unique events are allowed, based on the metrics-api constraints

    def test_unique_event__work_uri__measure_uri__timestamp__event_uri__where__country_uri_not_null_and_event_uri_not_null(self):
        """Test unique events can be saved without error, where both country
        and event uri are not null.
        """
        self._assert_valid_events(
            event_1__all_fields_present,
            event_2__all_fields_present
        )

    def test_unique_event__work_uri__measure_uri__timestamp__event_uri__where__country_uri_is_null_and_event_uri_not_null(self):
        """Test unique events can be saved without error, where country is
        null and event uri is not null.
        """
        self._assert_valid_events(
            event_1_duplicate_with_country_is_null,
            event_2_duplicate_with_country_is_null
        )

    def test_unique_event__work_uri__measure_uri__timestamp__event_uri__where__country_uri_is_null_and_event_uri_is_null(self):
        """Test unique events can be saved without error, where both country
        and event uri are null.
        """
        self._assert_valid_events(
            event_1_duplicate_with_both_event_uri_and_country_is_null,
            event_2_duplicate_with_both_event_uri_and_country_is_null
        )

    #  ## Assert that uniqueness is enforced, based on metrics-api constraints

    def test_enforced_unique_event__work_uri__measure_uri__timestamp__event_uri__where__country_uri_not_null_and_event_uri_not_null(self):
        """Test uniqueness is enforced where both country and event uri are
        not null.
        """
        self._assert_invalid_entries(
            event_1__all_fields_present,
            event_1_duplicate_with_different_value
        )

    def test_enforced_unique_event__work_uri__measure_uri__timestamp__event_uri__where__country_uri_is_null_and_event_uri_not_null(self):
        """Test uniqueness is enforced where country is null and event uri is
        not null.
        """
        self._assert_invalid_entries(
            event_3_country_is_null,
            event_3_duplicate_different_value,
        )

    def test_enforced_unique_event__work_uri__measure_uri__timestamp__event_uri__where__country_uri_is_null_and_event_uri_is_null(self):
        """Test uniqueness is enforced where both country and event are null."""
        self._assert_invalid_entries(
            event_4_country_is_null_event_uri_is_null,
            event_4_duplicate_different_value,
        )
