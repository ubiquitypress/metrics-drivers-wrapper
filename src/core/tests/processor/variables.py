from datetime import datetime

from core.settings import StaticProviders
from processor.models import Event, Scrape


now = datetime.now()


# ## Scrapes - all with non-configured driver settings

scrape_1 = Scrape(
    provider=StaticProviders.access_logs,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)
scrape_2 = Scrape(
    provider=StaticProviders.google_analytics,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)

scrape_3 = Scrape(
    provider=StaticProviders.crossref_cited_by,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)
scrape_4 = Scrape(
    provider=StaticProviders.access_logs_local,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)

scrape_6 = Scrape(
    provider=StaticProviders.google_books,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)

scrape_8 = Scrape(
    provider=StaticProviders.irus_uk,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)

scrape_9 = Scrape(
    provider=StaticProviders.unglue_it,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)

scrape_10 = Scrape(
    provider=StaticProviders.figshare,
    service_code='demo-does-not-exist',
    datestamp='2099-06-16',
)


# ## Event 1: Should be a unique event where all fields are present

event_1__all_fields_present = Event(
    work_uri='info:doi:10.13579/demo.001',
    value=3,
    event_uri='https://demo-site.com/value/1',
    country_uri='urn:iso:std:3166:-2:FR',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_1_duplicate_with_different_value = Event(
    work_uri='info:doi:10.13579/demo.001',
    value=5,
    event_uri='https://demo-site.com/value/1',
    country_uri='urn:iso:std:3166:-2:FR',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_1_duplicate_with_event_uri_is_null = Event(
    work_uri='info:doi:10.13579/demo.001',
    value=3,
    country_uri='urn:iso:std:3166:-2:FR',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_1_duplicate_with_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.001',
    value=3,
    event_uri='https://demo-site.com/value/1',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_1_duplicate_with_both_event_uri_and_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.001',
    value=3,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

# ## Event 2: Should be a unique event where all fields are present

event_2__all_fields_present = Event(
    work_uri='info:doi:10.13579/demo.0002',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_2_duplicate_with_event_uri_is_null = Event(
    work_uri='info:doi:10.13579/demo.0002',
    value=2,
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_2_duplicate_with_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.0002',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_2_duplicate_with_both_event_uri_and_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.0002',
    value=2,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

# ## Event 3: Country is null; event_uri is not null
event_3_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.0003',
    value=19,
    event_uri='https://more-demo-sites.com/value/3',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_3_duplicate_different_value = Event(
    work_uri='info:doi:10.13579/demo.0003',
    value=8,
    event_uri='https://more-demo-sites.com/value/3',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

# ## Event 4: Country is null; event_uri is null
event_4_country_is_null_event_uri_is_null = Event(
    work_uri='info:doi:10.13579/demo.0004',
    value=8,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_4_duplicate_different_value = Event(
    work_uri='info:doi:10.13579/demo.0004',
    value=11,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_5__all_fields_present = Event(
    work_uri='info:doi:10.13579/demo.0005',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_5_duplicate_with_event_uri_is_null = Event(
    work_uri='info:doi:10.13579/demo.0005',
    value=2,
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_5_duplicate_with_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.0005',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_5_duplicate_with_both_event_uri_and_country_is_null = Event(
    work_uri='info:doi:10.13579/demo.0005',
    value=2,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)


number_ranges = [
    (0, 98, '0'),
    (99, 193, '1'),
    (194, 255, '2'),
    (256, 331, '3'),
    (332, 398, '4'),
    (399, 436, '5'),
    (437, 447, '6'),
    (448, 485, '7'),
    (486, 563, '8'),
    (564, 623, '9'),
    (624, 634, '10'),
    (635, 728, '11'),
    (729, 809, '12'),
    (810, 877, '13'),
    (878, 940, '14'),
    (941, 1011, '15'),      # Test 1: 1000 | '15'
    (1012, 1042, '16'),
    (1043, 1141, '17'),
    (1142, 1209, '18'),
    (1210, 1240, '19'),
    (1241, 1301, '20'),
    (1302, 1385, '21'),
    (1386, 1440, '22'),
    (1441, 1495, '23'),
    (1496, 1581, '24'),
    (1582, 1677, '25'),
    (1678, 1766, '26'),
    (1767, 1839, '27'),
    (1840, 1858, '28'),
    (1859, 1906, '29'),
    (1907, 1949, '30'),
    (1950, 2016, '31'),     # Test 2: 2000 | '31'
    (2017, 2102, '32'),
    (2103, 2132, '33'),
    (2133, 2213, '34'),
    (2214, 2235, '35'),
    (2236, 2326, '36'),
    (2327, 2400, '37'),
    (2401, 2465, '38'),
    (2466, 2554, '39'),
    (2555, 2609, '40'),
    (2610, 2642, '41'),
    (2643, 2697, '42'),
    (2698, 2786, '43'),
    (2787, 2872, '44'),
    (2873, 2935, '45'),
    (2936, 2977, '46'),
    (2978, 2991, '47'),
    (2992, 3088, '48'),     # Test 3: 3000 | '48'
    (3089, 3127, '49'),
    (3128, 3189, '50'),
    (3190, 3285, '51'),
    (3286, 3347, '52'),
    (3348, 3392, '53'),
    (3393, 3454, '54'),
    (3455, 3514, '55'),
    (3515, 3539, '56'),
    (3540, 3615, '57'),
    (3616, 3667, '58'),
    (3668, 3727, '59'),
    (3728, 3740, '60'),
    (3741, 3801, '61'),
    (3802, 3821, '62'),
    (3822, 3859, '63'),
    (3860, 3925, '64'),
    (3926, 4004, '65'),     # Test 4: 4000 | '65'
    (4005, 4049, '66'),
    (4050, 4092, '67'),
    (4093, 4189, '68'),
    (4190, 4220, '69'),
    (4221, 4259, '70'),
    (4260, 4317, '71'),
    (4318, 4367, '72'),
    (4368, 4463, '73'),
    (4464, 4481, '74'),
    (4482, 4498, '75'),
    (4499, 4556, '76'),
    (4557, 4637, '77'),
    (4638, 4695, '78'),
    (4696, 4712, '79'),
    (4713, 4783, '80'),
    (4784, 4834, '81'),
    (4835, 4912, '82'),
    (4913, 4953, '83'),
    (4954, 5053, '84'),     # Test 5: 5000 | '84'
    (5054, 5151, '85'),
    (5152, 5217, '86'),
    (5218, 5253, '87'),
    (5254, 5339, '88'),
    (5340, 5353, '89'),
    (5354, 5419, '90'),
    (5420, 5484, '91'),
    (5485, 5547, '92'),
    (5548, 5641, '93'),
    (5642, 5688, '94'),
    (5689, 5775, '95'),
    (5776, 5846, '96'),
    (5847, 5866, '97'),
    (5867, 5938, '98'),
    (5939, 6028, '99')
]
