import unittest

from core.settings import StaticProviders
from processor.tasks import (
    get_process_plugin_function,
    process_plugin__access_logs,
    process_plugin__access_logs_local,
    process_plugin__crossref_cited_by,
    process_plugin__google_analytics,
    process_plugin__jstor,
    process_plugin__google_books,
    process_plugin__irus_uk,
    process_plugin__figshare,
)


class TasksTestCase(unittest.TestCase):

    def test_access_logs_function_fetched_by_get_process_plugin_function(self):
        """Test get_process_plugin_function finds expected functions."""

        plugin_function = get_process_plugin_function(
            StaticProviders.access_logs
        )
        self.assertEqual(
            plugin_function,
            process_plugin__access_logs,
            'Tasks - Incorrect function found for access logs driver.',
        )

    def test_access_logs_local_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__access_logs_local is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.access_logs_local
        )
        self.assertEqual(
            plugin_function,
            process_plugin__access_logs_local,
            'Tasks - Incorrect function found for access logs local driver.',
        )

    def test_crossref_cited_by_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__crossref_cited_by is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.crossref_cited_by
        )
        self.assertEqual(
            plugin_function,
            process_plugin__crossref_cited_by,
            'Tasks - Incorrect function found for access crossref_cited_by driver.',
        )

    def test_google_analytics_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__google_analytics is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.google_analytics
        )
        self.assertEqual(
            plugin_function,
            process_plugin__google_analytics,
            'Tasks - Incorrect function found for access google_analytics driver.',
        )

    def test_jstor_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__jstor is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.jstor
        )
        self.assertEqual(
            plugin_function,
            process_plugin__jstor,
            'Tasks - Incorrect function found for access jstor driver.',
        )

    def test_google_books_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__google_books is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.google_books
        )
        self.assertEqual(
            plugin_function,
            process_plugin__google_books,
            'Tasks - Incorrect function found for access google_books driver.',
        )

    def test_irus_uk_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__irus_uk is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.irus_uk
        )
        self.assertEqual(
            plugin_function,
            process_plugin__irus_uk,
            'Tasks - Incorrect function found for access irus_uk driver.',
        )

    def test_figshare_function_fetched_by_get_process_plugin_function(self):
        """Test process_plugin__figshare is fetched by get_process_plugin_function."""

        plugin_function = get_process_plugin_function(
            StaticProviders.figshare
        )
        self.assertEqual(
            plugin_function,
            process_plugin__figshare,
            'Tasks - Incorrect function found for access figshare driver.',
        )
