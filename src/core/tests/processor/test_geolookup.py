import unittest

from processor.geo_lookup import find_value_in_range

from .variables import number_ranges


class ProcessorGeoLookupTestCase(unittest.TestCase):

    def test_find_value_in_range_finds_value_in_range(self):
        """test find_value_in_range finds correct values in the range given."""
        test_values = [1000, 2000, 3000, 4000, 5000]

        test_results = [
            find_value_in_range(
                value=i,
                range_list=number_ranges
            ) for i in test_values
        ]

        expected_results = ['15', '31', '48', '65', '84']
        self.assertEqual(
            test_results,
            expected_results,
            'find_value_in_range - Incorrect results found.',
        )
