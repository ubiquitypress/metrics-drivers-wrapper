from pydantic import BaseModel
from typing import List, Optional


class UrlRegex(BaseModel):
    include: Optional[List[str]] = None
    exclude: Optional[List[str]] = None


class UrlSetting(BaseModel):
    segment: Optional[str] = None
    regex: Optional[UrlRegex] = UrlRegex()

    @property
    def vars(self):
        return dict(
            segment=self.segment,
            include_regexes=self.regex.include,
            exclude_regexes=self.regex.exclude,
        )
