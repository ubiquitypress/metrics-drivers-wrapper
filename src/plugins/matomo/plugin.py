from collections import defaultdict
from logging import getLogger

from matomo_driver import MatomoClient

from generic.mount_point import GenericDataProvider
from processor.models import Event

from .serializers import UrlSetting


logger = getLogger(__name__)


class MatomoProvider(GenericDataProvider):
    """Implement matomo integration."""

    def process(self, search_date, service_code, enforce_active=True):
        """Yield matomo events given a search date.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()

        timestamp = f'{search_date} 00:00:00'
        uri_schemes = self.set_uri_schemes(service_settings)
        hits = defaultdict(int)

        matomo = MatomoClient(
            base_url=service_settings["base_url"],
            site_id=service_settings["site_id"],
            token=service_settings["site_auth"],
        )
        method_params = dict(
            search_date=search_date,
            depth=service_settings.get('depth', 9)
        )

        # ## Get Views Metrics #########################
        views_config = service_settings.get('views', {})
        for measure_uri, view_settings in views_config.items():

            config = UrlSetting(**view_settings)
            method_params.update(**config.vars)

            for result in matomo.get_page_urls(**method_params):
                for identifier in self.multi_scheme_uri_to_id(
                        uri=result.url.rstrip('/'),
                        uri_schemes=uri_schemes,
                        uri_strict=self.uri_strict,
                ):
                    hits[(identifier['URI'], measure_uri)] += result.visits

        # ## Get Downloads Metrics #############################
        downloads_config = service_settings.get('downloads', {})
        for measure_uri, download_settings in downloads_config.items():

            config = UrlSetting(**download_settings)
            method_params.update(**config.vars)

            for result in matomo.get_downloads(**method_params):
                for identifier in self.multi_scheme_uri_to_id(
                        uri=result.url.rstrip('/'),
                        uri_schemes=uri_schemes,
                        uri_strict=self.uri_strict,
                ):
                    hits[(identifier['URI'], measure_uri)] += result.visits

        # ## Get Events Metrics #############################
        events_config = service_settings.get('events', {})
        for measure_uri, categories in events_config.items():
            for event in matomo.events_from_categories(search_date, categories):
                for identifier in self.multi_scheme_uri_to_id(
                        uri=event.identifier,
                        uri_schemes=uri_schemes,
                        uri_strict=self.uri_strict,
                ):
                    hits[(identifier['URI'], measure_uri)] += event.visits

        for (work_uri, measure_uri), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri='',
                country_uri='',
                measure_uri=measure_uri,
                timestamp=timestamp,
            )
