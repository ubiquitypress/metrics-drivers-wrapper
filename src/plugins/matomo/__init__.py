__author__ = "Tai Jantarungsee"
__version__ = 0.1
__desc__ = "Provides Matomo metrics."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.MatomoProvider(
    measure_uri=None,
    provider=StaticProviders.matomo,
    setting_slug='matomo'
)
