__author__ = "Cristian Garcia"
__version__ = 0.1
__desc__ = "Provides usage data from the IRUS-UK API."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.IrusUkProvider(
    measure_uri="https://metrics.operas-eu.org/irus-uk/downloads/v1",
    provider=StaticProviders.irus_uk,
    setting_slug="irus_uk"
)
