from collections import defaultdict
from datetime import date
from itertools import product
from logging import getLogger

from irus_uk_driver import fetch_processed_report, has_required_settings

from generic.mount_point import GenericDataProvider
from processor.logic import is_last_day_of_month
from processor.models import Event


logger = getLogger(__name__)


class IrusUkProvider(GenericDataProvider):
    """Implements Irus UK API integration."""

    def prepare_credentials(
            self,
            search_date: str,
            service_code: str,
            enforce_active: bool,
    ):
        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()
        service_settings.update(begin_date=search_date, end_date=search_date)

        return service_settings

    def process(self, search_date, service_code, enforce_active=True):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to search, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        if not is_last_day_of_month(search_date):
            return logger.info(f"Skipping {search_date=} - not month end.")

        search_month = date.fromisoformat(search_date).strftime("%Y-%m")
        service_settings = self.prepare_credentials(
            search_month,
            service_code,
            enforce_active,
        )

        measure_uri = service_settings.pop("measure_uri", self.measure_uri)
        uri_schemes = self.set_uri_schemes(service_settings)
        timestamp = f"{search_date} 00:00:00"
        hits = defaultdict(int)

        if not has_required_settings(**service_settings):
            raise ValueError(f"Invalid IRUS-UK settings {service_settings}")

        for item in fetch_processed_report(**service_settings):
            work_doi = f"info:doi:{item.doi.lower()}"
            identifiers = self.multi_scheme_uri_to_id(
                uri=work_doi,
                uri_schemes=uri_schemes,
                uri_strict=self.uri_strict,
            )
            for instance, identifier in product(item.instances, identifiers):
                country_uri = self.standardise_country(instance.country.code)
                work_country = (identifier["URI"], country_uri)
                hits[work_country] += instance.metrics.unique_requests

        for (work_uri, country_uri), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri="",
                country_uri=country_uri,
                measure_uri=measure_uri,
                timestamp=timestamp,
            )
