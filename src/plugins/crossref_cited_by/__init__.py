__author__ = "Rowan Hatherley"
__version__ = 0.1
__desc__ = "Provides DOI-based CrossRef Citation Data as a source for metrics."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.CrossrefCitedByProvider(
    measure_uri='https://metrics.operas-eu.org/crossref/citations/v1',
    provider=StaticProviders.crossref_cited_by,
    setting_slug='xref_cited_by'
)
