from logging import getLogger

from crossref_citedby_driver import CrossrefCitedByClient

from flask import current_app

from generic.mount_point import GenericDataProvider
from processor.logic import get_uri_schema_values
from processor.models import Event


logger = getLogger(__name__)


class CrossrefCitedByProvider(GenericDataProvider):
    """ Implements Crossref Cited-by API integration. """

    def prepare_credentials(self, search_date, service_code):

        service_settings = self.fetch_credentials(service_code).copy()
        service_settings.update(
            start_date=search_date,
            end_date=search_date,
        )
        return service_settings

    def process(self, search_date, service_code):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
        """

        crossref = CrossrefCitedByClient(
            tech_email=current_app.config['TOKENS_EMAIL']
        )
        credentials = self.prepare_credentials(search_date, service_code)
        uri_schemes = self.set_uri_schemes(credentials)

        raw_data, status = crossref.fetch_citation_xml(**credentials)
        citation_data = crossref.get_crossref_citations(xml_content=raw_data)

        for doi, citation_entries in citation_data.items():
            doi_schema, doi_value = get_uri_schema_values('doi', doi)
            for identifier in self.multi_scheme_uri_to_id(
                    uri=doi_value,
                    uri_schemes=uri_schemes,
                    uri_strict=self.uri_strict,
            ):
                work_uri = identifier['URI']
                for entry in citation_entries:
                    cited_by, timestamp = crossref.get_citation_data(entry)

                    yield Event(
                        work_uri=work_uri,
                        value=1,
                        event_uri=cited_by,
                        country_uri='',
                        measure_uri=self.measure_uri,
                        timestamp=timestamp,
                    )
