__author__ = "Cristian Garcia"
__version__ = 0.1
__desc__ = "Provides Figshare Data as a source for metrics."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.FigshareProvider(
    measure_uri='',
    provider=StaticProviders.figshare,
    setting_slug='figshare'
)
