from datetime import datetime
from collections import defaultdict
from logging import getLogger
import os

from figshare_driver import fetch_report

from generic.mount_point import GenericDataProvider

from processor.logic import (
    get_uri_schema_values,
    is_last_day_of_month,
    first_day_of_month,
    country_name_to_iso
)
from processor.models import Event


logger = getLogger(__name__)


class FigshareProvider(GenericDataProvider):
    """Implement figshare integration."""
    def prepare_credentials(
            self,
            search_date: str,
            service_code: str,
            enforce_active: bool
    ):
        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()

        service_settings.update(
            start_date=first_day_of_month(search_date),
            end_date=search_date
        )
        return service_settings

    @staticmethod
    def load_data(service_settings, service_code):
        base_logdir = service_settings.get('logdir', 'plugins/figshare/uploads')
        log_dir = os.path.join(base_logdir, service_code)
        ids_file = service_settings.get('ids_file', 'article_ids.txt')

        # Load article IDs from file
        with open(os.path.join(log_dir, ids_file), 'r') as file:
            article_ids = [int(line.strip()) for line in file]
        return fetch_report(
            service_settings,
            article_ids
        )

    def process(self, search_date, service_code, enforce_active=True):
        """Get figshare events on a given search date.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        if not is_last_day_of_month(search_date):
            return logger.info(f'Skipping {search_date=} - not month end.')

        service_settings = self.prepare_credentials(
            search_date,
            service_code,
            enforce_active,
        )

        result = self.load_data(service_settings, service_code)

        date_obj = datetime.strptime(search_date, "%Y-%m-%d")
        formatted_date = date_obj.strftime("%Y-%m")

        hits = defaultdict(int)
        uri_schemes = self.set_uri_schemes(service_settings)
        for counter in result:
            for item in counter:
                _, doi_value = get_uri_schema_values(
                    'doi', item['doi']
                )
                for identifier in self.multi_scheme_uri_to_id(
                        uri=doi_value,
                        uri_schemes=uri_schemes,
                        uri_strict=self.uri_strict,
                ):
                    work_uri = identifier['URI']

                if item['Metrics']['breakdown']:
                    countries = item['Metrics']['breakdown'][formatted_date]
                    for country, metrics in countries.items():
                        country_uri = self.standardise_country(
                            country_name_to_iso(country)
                        )
                        key = (
                            work_uri,
                            country_uri,
                            search_date,
                            item.get('measure')
                        )
                        value = metrics.get('total', 0)

                        if value > 0:
                            hits[key] += value

        for (work_uri, country_uri, timestamp, measure), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri='',
                country_uri=country_uri,
                measure_uri=measure,
                timestamp=timestamp,
            )
