__author__ = "Cristian Garcia"
__version__ = 0.1
__desc__ = ""

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.UnglueItProvider(
    measure_uri="https://metrics.operas-eu.org/unglueit/downloads/v1",
    provider=StaticProviders.unglue_it,
    setting_slug="unglue_it"
)
