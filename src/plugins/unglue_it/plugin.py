from collections import defaultdict
from logging import getLogger

from unglue_it_driver import fetch_results

from core import db
from generic.mount_point import GenericDataProvider
from processor.logic import get_uri_schema_values, is_last_day_of_month
from processor.models import Event


logger = getLogger(__name__)


class UnglueItProvider(GenericDataProvider):
    """Implement unglue.it integration."""

    def check_new_downloads(self, total_downloads: int, work_uri: str) -> int:
        """Calculate new downloads for a work_uri by comparing total downloads
        to values already present in the database.

        Args:
            total_downloads (int): Downloads returned by the Unglue.it API.
            work_uri (str): Work uri returned by the translator.

        Returns:
            int: New downloads for the work_uri.
        """
        values = db.session.query(Event.value).filter(
            Event.measure_uri == self.measure_uri, Event.work_uri == work_uri
        ).all()
        values = sum([i[0] for i in values])
        result = total_downloads - values

        return max(result, 0)

    def process(
        self, search_date: str, service_code: str, enforce_active: bool = True
    ):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to search, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        if not is_last_day_of_month(search_date):
            return logger.info(f"Skipping {search_date=} - not month end.")

        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()

        timestamp = f"{search_date} 00:00:00"
        uri_schemes = self.set_uri_schemes(service_settings)

        hits = defaultdict(int)
        for result in fetch_results(**service_settings):
            _, isbn_uri = get_uri_schema_values("isbn", result.isbn)
            for identifier in self.multi_scheme_uri_to_id(
                uri=isbn_uri,
                uri_schemes=uri_schemes,
                uri_strict=self.uri_strict,
            ):
                work_uri = identifier["URI"]
                hits[work_uri] += result.downloads

        for work_uri, value in hits.items():
            if new_downloads := self.check_new_downloads(value, work_uri):
                yield Event(
                    work_uri=work_uri,
                    value=new_downloads,
                    event_uri="",
                    country_uri="",
                    measure_uri=self.measure_uri,
                    timestamp=timestamp,
                )
