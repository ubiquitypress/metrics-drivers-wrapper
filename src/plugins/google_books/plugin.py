from collections import defaultdict
from logging import getLogger
import os
from typing import Dict, List

from selenium.common.exceptions import (
    ElementNotInteractableException,
    InvalidSessionIdException,
    WebDriverException
)

from generic.mount_point import GenericDataProvider, DriverNotReadyException
from google_books_driver import fetch_report, extract_report_content
from plugins.google_books.logic import load_data_from_file
from processor.logic import (
    get_next_day, get_uri_schema_values, is_last_day_of_month
)
from processor.models import Event

logger = getLogger(__name__)


class GoogleBooksProvider(GenericDataProvider):
    """Implements Google books integration."""

    def prepare_credentials(
            self,
            search_date: str,
            service_code: str,
            enforce_active: bool
    ) -> Dict:

        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()

        service_settings.update(
            start_date=search_date,
            end_date=get_next_day(search_date),
        )

        return service_settings

    @staticmethod
    def get_csv_results(
            service_code: str, service_settings: dict
    ) -> List[Dict]:
        """
        Bridge to out driver,
        Option 1: Extract the csv_data checking if the file name convention
        is correct with the date and matches the desired search_date
        or end_date.
        Option 2: Fetch the report using webscraping from Google and
        return a list of dict.
        Note: Both options will rely on the 'extract_report_content' method.

        Args:
            service_code (str): Service code to load the files from.
            service_settings (dict): Driver config.

        Returns:
            List[Dict]: List of a dict holding the report data.
        """
        if uploads_directory := service_settings.get("uploads_directory"):
            log_dir = os.path.join(uploads_directory, service_code)
            search_date = service_settings.get("start_date")
            csv_data = load_data_from_file(search_date, log_dir)

            if csv_data is None:
                raise DriverNotReadyException('No valid month available.')

        else:
            for i in range(3):
                try:
                    csv_data = fetch_report(**service_settings)
                    break
                except (
                    ElementNotInteractableException,
                    InvalidSessionIdException,
                    UnicodeDecodeError,
                    WebDriverException
                ) as e:
                    exception = e
                    continue
            else:
                logger.error("All retries exceeded - terminating plugin.")
                raise exception

        return extract_report_content(
            csv_data,
            service_settings.get("expected_headers"),
        )

    def process(
            self,
            search_date: str,
            service_code: str,
            enforce_active: bool = True,
    ):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        service_settings = self.prepare_credentials(
            search_date,
            service_code,
            enforce_active,
        )
        uri_schemes = self.set_uri_schemes(service_settings)

        if not is_last_day_of_month(search_date) and service_settings.get(
                "uploads_directory"
        ):  # Only accept last day if uploading a file
            return logger.info(f'Skipping {search_date=} - not month end.')

        timestamp = f'{search_date} 00:00:00'
        results = self.get_csv_results(service_code, service_settings)

        hits = defaultdict(int)
        views_measure = 'https://metrics.operas-eu.org/google-books/views/v1'
        reads_measure = 'https://metrics.operas-eu.org/google-books/reads/v1'
        for result in results:
            _, isbn_uri = get_uri_schema_values(
                'isbn', result['Primary ISBN']
            )
            views = result['Book Visits (BV)']
            views = int(views.replace(",", "")) if views else None
            reads = result['BV with Pages Viewed']
            reads = int(reads.replace(",", "")) if reads else None
            for identifier in self.multi_scheme_uri_to_id(
                    uri=isbn_uri,
                    uri_schemes=uri_schemes,
                    uri_strict=self.uri_strict,
            ):
                work_uri = identifier['URI']
                if views:
                    hits[(work_uri, views_measure)] += views
                if reads:
                    hits[(work_uri, reads_measure)] += reads

        for (work_uri, measure_uri), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri='',
                country_uri='',
                measure_uri=measure_uri,
                timestamp=timestamp,
            )
