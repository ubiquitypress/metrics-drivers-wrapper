__author__ = "Cristian Garcia"
__version__ = 0.1
__desc__ = ""

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.GoogleBooksProvider(
    measure_uri=None,  # various measures, depending on metric.
    provider=StaticProviders.google_books,
    setting_slug='google_books'
)
