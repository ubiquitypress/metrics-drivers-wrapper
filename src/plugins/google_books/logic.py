import os


def load_data_from_file(search_date: str, log_dir: str) -> str:
    """
    Loop through files in logdir/service_code to determine the
    first occurrence by date, the file name convention:
    GoogleBooksTrafficReport-YYY-mm-to-YYYY-mm.csv.
    Returns:
        str: Content of the first file occurrence matching search_month.
    """
    for file_name in sorted(os.listdir(log_dir)):
        dir_and_file = os.path.join(log_dir, file_name)
        if dir_and_file.endswith(f'{search_date[:7]}.csv'):
            with open(dir_and_file, "r", encoding="utf-16") as file:
                return file.read()
