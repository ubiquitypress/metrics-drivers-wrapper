__author__ = "Cristian Garcia"
__version__ = 0.1
__desc__ = "Provides filtered metrics from Access Logs Local."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.AccessLogsLocalProvider(
    measure_uri=None,
    provider=StaticProviders.access_logs_local,
    setting_slug="access_logs_local",
)
