from datetime import datetime
from logging import getLogger

from access_logs_local_driver import make_filters, LogStream
from access_logs_local_driver.geolookup import GeoLookup
from generic.mount_point import GenericDataProvider
from processor.models import Event
from bots_loader.bots_loader import BotsDetector


logger = getLogger(__name__)


class AccessLogsLocalProvider(GenericDataProvider):
    """Implements Access-logs-local API integration."""

    def prepare_credentials(
            self,
            search_date: str,
            service_code: str,
            enforce_active: bool
    ):
        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()
        service_settings.update(start_date=search_date, end_date=search_date)
        return service_settings

    @staticmethod
    def timestamp_to_datestamp(timestamp):
        return datetime(
            timestamp.year,
            timestamp.month,
            timestamp.day,
        )

    def get_logs(self, service_settings: dict) -> LogStream:
        """Bridge to out driver, prepare the data making the filters first,
        and then call the class LogStream to get the logs results.

        Args:
            service_settings (dict): Driver config.

        Returns:
            LogStream: Iterable of logs.
        """

        filter_groups = [
            (
                measure_regex["measure"],
                make_filters(
                    measure_regex["regex"],
                    service_settings["excluded_ips"],
                ),
                measure_regex["regex"],
            ) for measure_regex in service_settings['measure_regexes']
        ]

        return LogStream(
            log_dir=service_settings['logdir'],
            filter_groups=filter_groups,
            url_prefix=service_settings["url_prefix"],
            start_date=service_settings["start_date"],
            end_date=service_settings["end_date"],
        )

    def process(self, search_date, service_code, enforce_active=True):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        service_settings = self.prepare_credentials(
            search_date,
            service_code,
            enforce_active,
        )
        uri_schemes = self.set_uri_schemes(service_settings)

        hits, session = {}, {}
        geo = GeoLookup(None)
        logs = self.get_logs(service_settings)

        bots_detector = BotsDetector()
        for measure_uri, (time_str, ip_address, url, agent) in logs:
            if bots_detector.is_bot(agent):
                continue

            timestamp = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
            date = f"{self.timestamp_to_datestamp(timestamp).date()} 00:00:00"
            country_uri = geo.lookup_country(ip_address, date)

            for identifier in self.multi_scheme_uri_to_id(
                    uri=url,
                    uri_schemes=uri_schemes,
                    uri_strict=self.uri_strict,
            ):
                work_uri = identifier.get('URI')
                browser = (measure_uri, ip_address, agent, work_uri)

                if browser in session:
                    last = session[browser]

                    if service_settings.get('rollover'):
                        session[browser] = timestamp

                    offset = (timestamp - last).seconds
                    if offset < service_settings.get('session_timeout'):
                        continue

                session[browser] = timestamp

                key = (measure_uri, date, work_uri, country_uri)
                if key not in hits:
                    hits[key] = 0

                hits[key] += 1

        for (measure_uri, date, work_uri, country_uri), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri="",
                country_uri=country_uri,
                measure_uri=measure_uri,
                timestamp=date,
            )
