from collections import OrderedDict, defaultdict
from logging import getLogger

from core import consul
from generic.mount_point import GenericDataProvider
from processor.logic import get_next_day
from processor.models import Event

from .logic import (
    aggregate_metrics,
    calculate_request_hosts,
    create_traefik_filters,
    organise_logs,
)


logger = getLogger(__name__)


class AccessLogsProvider(GenericDataProvider):
    """ Implements Crossref Cited-by API integration. """

    def prepare_credentials(self, search_date, service_code, enforce_active):
        """Provide credentials and filters for GA logs."""

        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()

        measure_regexes = OrderedDict(
            (v['measure'], v['regex']) for _, v in
            consul.ordered_items(service_settings['measure_regexes'])
        )

        service_settings.update(
            measure_regexes=measure_regexes,
            start_date=search_date,
            end_date=get_next_day(search_date),
        )

        filters = [key for key in service_settings if key.endswith('_filter')]
        for key in filters:
            filter_key = key[:-len('_filter')]  # trying to be explicit
            service_settings[filter_key] = service_settings.pop(key)

        return service_settings

    def process(self, search_date, service_code, enforce_active=True):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        service_settings = self.prepare_credentials(
            search_date,
            service_code,
            enforce_active,
        )
        uri_schemes = self.set_uri_schemes(service_settings)

        key_file_content = service_settings.pop('json_key')
        measure_regexes = service_settings.pop('measure_regexes')

        resource_type = service_settings.get('resource_type', "k8s_container")
        filters = create_traefik_filters(**service_settings)

        access_logs = organise_logs(key_file_content, filters, resource_type)
        aggregated_metrics = aggregate_metrics(access_logs, measure_regexes)

        timestamp = f'{search_date} 00:00:00'
        request_hosts = calculate_request_hosts(
            request_host_filter=service_settings['request_host']
        )
        event_dict = {}

        for measure_uri, aggregations in aggregated_metrics.items():
            for (path, country_code), value in aggregations.items():
                country_uri = self.standardise_country(country_code)
                for request_host in request_hosts:
                    url = f'https://{request_host}{path}'
                    identifiers = self.multi_scheme_uri_to_id(
                        uri=url,
                        uri_schemes=uri_schemes,
                        uri_strict=self.uri_strict,
                    )
                    for identifier in identifiers:
                        work_uri = identifier['URI']
                        work_country = (work_uri, country_uri)

                        if work_country not in event_dict:
                            event_dict[work_country] = defaultdict(int)

                        event_dict[work_country][measure_uri] += value

                    if identifiers:
                        break  # Match paths to one request host, max.

        for (work_uri, country_uri), measure_dict in event_dict.items():
            for measure_uri, value in measure_dict.items():

                yield Event(
                    work_uri=work_uri,
                    value=value,
                    event_uri='',
                    country_uri=country_uri,
                    measure_uri=measure_uri,
                    timestamp=timestamp,
                )
