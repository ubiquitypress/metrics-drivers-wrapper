"""Logic required by the Access Logs driver/plugin.

In theory this will one day be part of the access logs driver python library.
"""

from collections import defaultdict, OrderedDict
from datetime import timedelta
import re
from urllib.parse import urlparse

from google.cloud import logging
from google.oauth2.service_account import Credentials

from bots_loader.bots_loader import BotsDetector
from processor.geo_lookup import lookup_country_from_ip


def create_traefik_filters(
    start_date,
    end_date,
    resource_type="k8s_container",
    traefik_namespace="default",
    request_host="example.com",
    request_path='"/path/to/books/10" OR "/path/to/chapters/10"',
    cluster_name="default-cluster",
    project_id="default-project",
    zone="default-zone",
    request_url=None,
):
    """Create filters to submit to GKE when querying access logs.

    Args:
        start_date (str): YYYY-MM-DD
        end_date (str): YYYY-MM-DD
        resource_type (str): "k8s_container" or "http_load_balancer"
        traefik_namespace (str): | "k8s_container", only
        request_host (str):  | "k8s_container", only
        request_path (str): | "k8s_container", only
        cluster_name (str): | "k8s_container", only
        project_id (str): | "k8s_container", only
        zone (str): | "k8s_container", only
        request_url (str): Full URLs to search | http_load_balancer, only

    Returns:
        str: Filter string to be passed to the logs explorer.

    Note: This function currently works with two different resource types. One
    for "k8s_container" and the other "http_load_balancer".
    """

    filters_list = [
        f'timestamp>="{start_date}T00:00:00Z"',  # scrape
        f'timestamp<"{end_date}T00:00:00Z"',
        f'resource.type="{resource_type}"',
        'severity="INFO"',  # Generic from this point
    ]

    if resource_type == 'k8s_container':
        filters_list.extend([
            f'resource.labels.namespace_name="{traefik_namespace}"',
            f'resource.labels.cluster_name="{cluster_name}"',  # cluster/project
            f'resource.labels.project_id="{project_id}"',
            f'resource.labels.location="{zone}"',
            f'jsonPayload.RequestHost:({request_host})',
            f'jsonPayload.RequestPath:{request_path}',
            'jsonPayload.RequestMethod="GET" OR "POST"',
            '-jsonPayload.request_User-Agent:"+http"',
        ])

    elif resource_type == 'http_load_balancer':
        filters_list.extend([
            f'httpRequest.requestUrl: {request_url}',
            'httpRequest.requestMethod="GET" OR "POST"',
            '-httpRequest.userAgent:"+http"',
        ])

    else:
        raise ValueError(f'Invalid value provided - {resource_type=}')

    return "\n".join(filters_list)


def fetch_logs(key_file_content, filters):
    """Query GKE for logs."""
    credentials = Credentials.from_service_account_info(key_file_content)
    logging_client = logging.Client(
        project=credentials.project_id,
        credentials=credentials,
    )

    for log in logging_client.list_entries(filter_=filters, page_size=5000):
        yield log


def remove_overlaps(datetime_list, session_timeout=30):
    """Remove duplicate page visits that occur within the same sessions.

    Args:
        datetime_list: Datetimes during which page visits occurred.
        session_timeout: Minutes until a session is considered closed.

    Returns:
        list: All session datetimes from the start of each session.
    """
    datetime_list.sort()
    last_session_visit = datetime_list[0]
    new_list = [last_session_visit]

    for timestamp in datetime_list[1:]:
        range_end = last_session_visit + timedelta(minutes=session_timeout)
        if timestamp >= range_end:
            new_list.append(timestamp)
        last_session_visit = timestamp

    return new_list


def get_log_info(log_object, resource_type='k8s_container'):
    if resource_type == 'k8s_container':
        log = log_object.payload
        user_agent = log.get("request_User-Agent", "")
        client_ip = log["ClientHost"]
        request_path = urlparse(log["RequestPath"]).path.rstrip("/")

    elif resource_type == 'http_load_balancer':
        log = log_object.http_request
        user_agent = log.get("userAgent", "")
        client_ip = log["remoteIp"]
        request_path = urlparse(log["requestUrl"]).path.rstrip("/")

    else:
        raise ValueError(f'Invalid value provided - {resource_type=}')

    return user_agent, client_ip, request_path, log_object.timestamp


def organise_logs(key_file_content, filters, resource_type='k8s_container'):
    """Organise log entries, based on client IP, for each URL path capturing
    the timestamps of when users visited that URL.
    """
    detect_bots = BotsDetector()
    logs_dict = {}

    for log in fetch_logs(key_file_content, filters):
        user_agent, client_ip, request_path, timestamp = get_log_info(
            log_object=log,
            resource_type=resource_type,
        )
        if detect_bots.is_bot(user_agent):
            continue

        ip_dict = logs_dict.setdefault(client_ip, defaultdict(list))
        ip_dict[request_path].append(timestamp)
        logs_dict[client_ip].update(ip_dict)

    return logs_dict


def remove_submatch(pattern, string, group_name):
    """Remove substring from a regex match, based on its group name.

    Args:
        pattern (re.Pattern): Compiled regex pattern.
        string (str): String that the regex is searching.
        group_name (str): Name of the group to remove.

    Returns:
        str: Matched string with the substring removed.
    """
    match = pattern.match(string)
    if match:
        if group_name in pattern.groupindex.keys():
            _start, _end = match.span(group_name)
            return match.string[:_start] + match.string[_end:]
        return match.string
    return ''


def aggregate_metrics(access_logs_dict, regexes):
    """Order metrics in a dict, based on client IP address, then split
    metrics into different measures, based on URL regex.
    """
    if not isinstance(regexes, OrderedDict):
        raise TypeError(f"regexes must be an OrderedDict, not {type(regexes)}")

    metrics_dict = {}

    for measure, regex in regexes.items():
        metrics_dict[measure] = defaultdict(int)
        search_pattern = re.compile(regex)

        for client_ip, path_dict in access_logs_dict.items():
            country_code = lookup_country_from_ip(client_ip)
            paths = tuple(path_dict.keys())

            for path in paths:
                if search_pattern.fullmatch(path):
                    sub_path = remove_submatch(search_pattern, path, 'remove')
                    path_country = (sub_path, country_code)
                    timestamps = access_logs_dict[client_ip].pop(path)
                    metrics_dict[measure][path_country] += len(
                        remove_overlaps(timestamps)
                    )

    return metrics_dict


def calculate_request_hosts(request_host_filter):
    return list(
        map(
            lambda x: x.strip('" '),
            request_host_filter.split(' OR '),
        )
    )
