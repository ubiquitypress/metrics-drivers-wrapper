__author__ = "Rowan Hatherley"
__version__ = 0.1
__desc__ = "Provides Download, Reads and Page Views metrics from Access Logs."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.AccessLogsProvider(
    measure_uri=None,  # Several measure for this plugin
    provider=StaticProviders.access_logs,
    setting_slug='access_logs'
)
