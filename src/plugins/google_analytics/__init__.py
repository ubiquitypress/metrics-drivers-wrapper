__author__ = "Rowan Hatherley"
__version__ = 0.1
__desc__ = "Provides Google Analytics Views metrics."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.GoogleAnalyticsProvider(
    measure_uri=None,  # various measures, depending on metric.
    provider=StaticProviders.google_analytics,
    setting_slug='google_analytics'
)
