from collections import defaultdict
import logging
from logging import getLogger

from google_analytics_driver import (
    GAClient,
    build_ga_request,
    generate_row_data,
    get_statistics,
    initialize_service,
)

from generic.mount_point import GenericDataProvider
from processor.client import new_translator_client
from processor.models import Event


getLogger('googleapicliet.discovery_cache').setLevel(logging.ERROR)
logger = getLogger(__name__)


class GoogleAnalyticsProvider(GenericDataProvider):
    """Implement Google Analytics integration."""

    def prepare_credentials(self, search_date, service_code, enforce_active):

        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()
        service_settings.update(start_date=search_date, end_date=search_date)

        return service_settings

    def prepare_client(self):
        return GAClient(
            translator=new_translator_client(),
            uri_scheme=self.uri_scheme,
            uri_strict=self.uri_strict,
            country_uri_scheme=self.country_uri_scheme,
        )

    def process(self, search_date, service_code, enforce_active=True):
        """Process a service on a specified day to get events.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        ga_client = self.prepare_client()
        credentials = self.prepare_credentials(
            search_date,
            service_code,
            enforce_active
        )

        if 'uri_scheme' in credentials:
            ga_client.uri_scheme = credentials.pop('uri_scheme')

        prefix = credentials.pop('prefix')
        regexes = credentials.pop('regex')

        service = initialize_service(  # Initialise GA client and build request
            key_file_content=credentials.pop('json_key')
        )

        timestamp = f'{search_date} 00:00:00'

        # At this point we need to separate event vs view measures

        # View keys
        view_settings = credentials.pop('views')
        view_dimensions = view_settings['dimensions']
        view_metrics = view_settings['metrics']

        # TODO: Expand to use multiple view measures
        view_measure = view_settings['measures']['sessions']['measure']

        # Event keys
        event_settings = credentials.pop('events')
        measures = event_settings['measures']
        event_measures = list(measures.values())

        event_dimensions = event_settings['dimensions']
        event_metrics = event_settings['metrics']

        event_headers = event_settings.pop('headers', None)
        event_identifier_type = event_settings.pop('identifier_type', 'https')

        # Keys established

        request = build_ga_request(
            metrics=view_metrics,
            dimensions=view_dimensions,
            **credentials
        )

        results = generate_row_data(get_statistics(service, request))
        next(results)  # remove unused header

        hits = ga_client.resolve_hits(results, prefix=prefix, regexes=regexes)

        for (work_uri, country_uri), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri='',
                country_uri=country_uri,
                measure_uri=view_measure,
                timestamp=timestamp,
            )

        credentials.update(filters=[])
        request = build_ga_request(
            metrics=event_metrics,
            dimensions=event_dimensions,
            **credentials
        )

        results = generate_row_data(
            get_statistics(service, request),
            headers=event_headers
        )

        # ## TODO: Neaten up code below this line  ##

        headers = next(results)

        events_dict = defaultdict(list)
        first_measure = event_measures[0]

        if 'categories' not in first_measure:  # Assume no categories
            events_dict[first_measure['measure']] = results

        else:
            category_index = headers.index('ga:eventCategory')
            for result in results:
                event_category = result[category_index]
                for settings_entry in event_measures:
                    if event_category in settings_entry['categories']:
                        events_dict[settings_entry['measure']].append(result)
                        break

        # ## TODO: Neaten up code above this line even more... ##
        # ## TODO: And below this line . . . ##

        hit_kwargs = {}

        if event_identifier_type == 'https':
            hit_kwargs.update(
                prefix=prefix,
                regexes=regexes
            )

        for measure, results in events_dict.items():
            hits = ga_client.resolve_hits(
                results,
                event_identifier_type,
                **hit_kwargs
            )

            for (work_uri, country_uri), value in hits.items():
                yield Event(
                    work_uri=work_uri,
                    value=value,
                    event_uri='',
                    country_uri=country_uri,
                    measure_uri=measure,
                    timestamp=timestamp,
                )
