__author__ = "Tai Jantarungsee"
__version__ = 0.1
__desc__ = "Provides Jstor views metrics."

from core.settings import StaticProviders

from . import plugin


PROVIDER = plugin.JstorProvider(
    measure_uri='https://metrics.operas-eu.org/jstor/usage/v1',  # various measures, depending on metric.
    provider=StaticProviders.jstor,
    setting_slug='jstor'
)
