from collections import defaultdict
from datetime import date
from logging import getLogger
import os

from generic.mount_point import DriverNotReadyException, GenericDataProvider
from processor.models import Event
from processor.logic import is_last_day_of_month, country_name_to_iso

from .logic import (
    load_data_from_file,
    standardise_uri,
)


logger = getLogger(__name__)


class JstorProvider(GenericDataProvider):
    """Implement jstor integration."""

    def prepare_credentials(self, search_date, service_code, enforce_active):
        service_settings = self.fetch_credentials(
            service_code,
            enforce_active,
        ).copy()

        return service_settings

    def process(self, search_date, service_code, enforce_active=True):
        """Get jstor events on a given search date.

        Args:
            search_date (str): Date to start searching from, as YYYY-MM-DD.
            service_code (str): Service we are collecting metrics for.
            enforce_active (bool): Set to False for testing inactive settings.
        """
        if not is_last_day_of_month(search_date):
            return logger.info(f'Skipping {search_date=} - not month end.')

        service_settings = self.prepare_credentials(
            search_date,
            service_code,
            enforce_active,
        )

        search_month = date.fromisoformat(search_date).strftime('%b-%Y')
        base_logdir = service_settings.get('logdir', 'plugins/jstor/uploads')
        logdir = os.path.join(base_logdir, service_code)
        sheet_name = service_settings.get('sheet_name')

        dataset = load_data_from_file(search_month, logdir, sheet_name)
        if dataset is None:
            raise DriverNotReadyException('No valid month available.')

        hits = defaultdict(int)
        uri_schemes = self.set_uri_schemes(service_settings)

        for _, row in dataset.iterrows():
            isbn = standardise_uri(row["eISBN"])
            for identifier in self.multi_scheme_uri_to_id(
                    uri=isbn,
                    uri_schemes=uri_schemes,
                    uri_strict=self.uri_strict,
            ):
                work_uri = identifier['URI']
                country_uri = self.standardise_country(
                    country_name_to_iso(row.get('Country Name'))
                )
                if value := int(row[search_month]):  # Ignore `0` views
                    key = (work_uri, country_uri, search_date)
                    hits[key] += value

        for (work_uri, country_uri, timestamp), value in hits.items():
            yield Event(
                work_uri=work_uri,
                value=value,
                event_uri='',
                country_uri=country_uri,
                measure_uri=self.measure_uri,
                timestamp=timestamp,
            )
