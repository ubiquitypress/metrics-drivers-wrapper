import logging
import os
import re

import pandas as pd
from pandas.core.frame import DataFrame


month_year_regex = re.compile(r'\w{3}-\d{4}')  # Matches MMM-YYYY format


def standardise_uri(uri):
    return f'urn:isbn:{uri}'


def load_data_from_file(
        search_month: str,
        log_dir: str,
        sheet_name: str = None,
) -> DataFrame:
    """Loop through files in logdir to determine the relevant one by date.

    Args:
        search_month (str): Name of month column containing metrics (MMM-YYYY).
        log_dir (str): Directory containing JSTOR files.
        sheet_name (str): Name of sheet in xlsx file that contains metrics.

    Returns:
        DataFrame: First file occurrence matching search_month.
    """
    for file_name in sorted(os.listdir(log_dir)):
        dir_and_file = os.path.join(log_dir, file_name)
        df = pd.read_excel(
            dir_and_file,
            sheet_name=sheet_name,
            skiprows=range(8),  # Skip the first 8 rows
        )
        if isinstance(df, dict):  # If sheet name is not specified.
            df = next(iter(df.values()))

        date_columns = list(filter(month_year_regex.search, df.columns))
        if search_month in date_columns:
            logging.info("Returning DataFrame for: %s", file_name)
            return df
