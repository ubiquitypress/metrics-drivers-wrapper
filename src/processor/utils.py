from datetime import timedelta

from .models import Scrape
from .logic import queryset_exists


def determine_start_date(service_code, plugin):
    """Determine start date for creating new scrapes.

    Check if there is already a scrape for this plugin-service combo.
    If not, the start date can be retrieved from the plugin credentials.
    If it does already exist, the start date should be the latest.

    Args:
        service_code (str): Service we are collecting metrics for.
        plugin (GenericDataProvider): Plugin that will run for the scrapes.

    Returns:
        str: Date to start creating new scrapes from, in the form 'YYYY-MM-DD'.
    """
    scrape_query = Scrape.query.filter_by(
        service_code=service_code,
        provider=plugin.provider
    )
    if queryset_exists(scrape_query):
        latest = scrape_query.order_by(Scrape.datestamp.desc()).first()
        start_date = latest.datestamp + timedelta(days=1)
        return start_date.strftime('%Y-%m-%d')

    return plugin.fetch_credentials(service_code).get('start_date')
