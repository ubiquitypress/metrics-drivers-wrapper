from datetime import date, timedelta
from json import JSONDecodeError

from flask import current_app

from core import db, plugins, celery_app, consul
from core.settings import StaticProviders
from generic.mount_point import DriverConfigException, DriverNotReadyException
from processor.client import new_altmetrics_client

from .logic import generate_dates
from .models import Scrape, Event
from .utils import determine_start_date


def process_plugin(scrape_id):
    """Collect metrics plugin using arguments specified in the Scrape."""

    scrape = Scrape.query.get(scrape_id)
    plugin = plugins.get_plugin(scrape.provider).PROVIDER
    datestamp = scrape.datestamp.strftime('%Y-%m-%d')

    event_list = plugin.process(
        search_date=datestamp,
        service_code=scrape.service_code
    )

    try:
        for event in event_list:
            if event.is_unique():
                db.session.add(event)
        db.session.commit()

        scrape.completed = True  # Now we can mark this scrape as successful.
        db.session.commit()

    except DriverConfigException as e:
        current_app.logger.info(e)
        current_app.logger.info(
            f'Deleting Scrape {scrape.service_code}; provider {scrape.provider}'
        )
        db.session.rollback()
        db.session.delete(scrape)
        db.session.commit()

    except DriverNotReadyException as e:
        current_app.logger.info(e)


@celery_app.task(name='process-plugin.access_logs')
def process_plugin__access_logs(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.access_logs_local')
def process_plugin__access_logs_local(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.crossref_cited_by')
def process_plugin__crossref_cited_by(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.google_analytics')
def process_plugin__google_analytics(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.jstor')
def process_plugin__jstor(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.google_books')
def process_plugin__google_books(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.irus_uk')
def process_plugin__irus_uk(*args, **kwargs):
    return process_plugin(*args, **kwargs)


@celery_app.task(name='process-plugin.figshare')
def process_plugin__figshare(*args, **kwargs):
    return process_plugin(*args, **kwargs)


PROCESS_PLUGIN_TASKS = {
    'process-plugin.access_logs': process_plugin__access_logs,
    'process-plugin.access_logs_local': process_plugin__access_logs_local,
    'process-plugin.crossref_cited_by': process_plugin__crossref_cited_by,
    'process-plugin.google_analytics': process_plugin__google_analytics,
    'process-plugin.jstor': process_plugin__jstor,
    'process-plugin.google_books': process_plugin__google_books,
    'process-plugin.irus_uk': process_plugin__irus_uk,
    'process-plugin.figshare': process_plugin__figshare,
}


def get_process_plugin_function(provider):
    """Determine which 'process_plugin' function to run for a given provider.

    Args:
        provider (enum): Provider for a given scrape.

    Returns:
        celery.local.PromiseProxy: Function to run for plugin task.
    """
    task_name = plugins.get_plugin_task_name(provider)
    return PROCESS_PLUGIN_TASKS[task_name]


@celery_app.task(name='pull-metrics')
def pull_metrics(**additional_filters):
    """Trigger pull metrics for all incomplete scrapes."""
    logs_cutoff = date.today() - timedelta(days=30)
    for scrape in Scrape.query.filter_by(completed=False, **additional_filters):

        if (  # remove/nullify logs scrapes triggered beyond 1 month threshold.
                scrape.provider == StaticProviders.access_logs
                and scrape.datestamp < logs_cutoff
        ):
            scrape.completed = True
            continue

        plugin_function = get_process_plugin_function(scrape.provider)
        plugin_function.delay(scrape_id=scrape.id)

    db.session.commit()  # commit nullified access logs scrapes if any.


@celery_app.task(name='create-scrapes')
def create_scrapes():
    """Create a scrape for all plugins and services."""

    chunk = 0  # commit Scrapes in chunks of 250
    service_codes = consul.client.kv.get_service_codes('metrics')

    for code in service_codes:
        for plugin_module in plugins.plugins_list:
            plugin = plugin_module.PROVIDER

            try:
                start_date = determine_start_date(code, plugin)
            except DriverConfigException:
                continue  # Skip plugins with no settings available.
            except (JSONDecodeError, Exception) as e:
                current_app.logger.info(
                    f'{type(e).__name__} - {e}: Check driver config of driver '
                    f'{code}-{plugin.setting_slug}.'
                )
                continue

            today = date.today().strftime('%Y-%m-%d')

            for day in generate_dates(start_date, cutoff_date=today):
                db.session.add(
                    Scrape(
                        provider=plugin.provider,
                        service_code=code,
                        datestamp=day
                    )
                )

                chunk += 1  # May want to do this in a different function.
                if chunk % 250 == 0:
                    current_app.logger.info("Creating 250 new scrapes")
                    db.session.commit()

    db.session.commit()


@celery_app.task(name='send-metrics')
def send_metrics():
    """Send all metrics collected over the past day to the metrics-api. """

    events_to_send = Event.query.filter_by(received_by_metrics_api=False)
    total_unsent = events_to_send.count()

    send_total = 500
    repeat_task = total_unsent > send_total  # Only send 500 Events at a time.

    metrics_api_url = f'{current_app.config.get("METRICS_API_BASE")}/events'
    client = new_altmetrics_client()
    try:  # Guard against request timeouts.
        for event in events_to_send.limit(send_total):
            data = dict(
                work_uri=event.work_uri,
                measure_uri=event.measure_uri,
                country_uri=event.country_uri,
                event_uri=event.event_uri,
                timestamp=event.timestamp.isoformat(),
                value=event.value,
            )
            response = client.post(metrics_api_url, json=data)
            if response.status_code in (200, 201):
                event.received_by_metrics_api = True

    except ConnectionResetError as e:
        current_app.logger.error(e)
        repeat_task = True

    except Exception as e:
        current_app.logger.exception(e)
        repeat_task = False

    finally:
        db.session.commit()

        if repeat_task:
            send_metrics.apply_async(countdown=30)
