from hirmeos_clients import AltmetricsClient, TokenClient, TranslatorClient

from flask import current_app


def new_translator_client():
    jwt_disabled = current_app.config['TESTING']
    return TranslatorClient(
        jwt_disabled=jwt_disabled,
        translator_api_base=current_app.config.get('TRANSLATION_API_BASE'),
        tokens_key=current_app.config.get('TOKENS_KEY'),
    )


def new_token_client():
    token_email = current_app.config.get('TOKENS_EMAIL')
    return TokenClient(
        tokens_key=current_app.config.get('TOKENS_KEY'),
        tokens_email=token_email,
        tokens_account=f'acct:{token_email}',
        tokens_name='tech admin',
    )


def new_altmetrics_client():
    return AltmetricsClient(
        email=current_app.config.get('ALTMETRICS_USER'),
        password=current_app.config.get('ALTMETRICS_PASSWORD'),
    )
