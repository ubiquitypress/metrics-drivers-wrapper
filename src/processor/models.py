import arrow
from sqlalchemy.schema import UniqueConstraint

from core.database import (
    Boolean,
    Column,
    Date,
    DateTime,
    Integer,
    Model,
    String,
)
from core.logic import get_enum_by_value
from core.settings import StaticProviders
from processor.logic import queryset_exists


class Event(Model):
    def __init__(self, *args, **kwargs):
        """Convert the date string to Datetime and strip the time zone as
        newer versions of SqlAlquemy don't allow timezone-aware."""
        super().__init__(*args, **kwargs)
        self.timestamp = arrow.get(self.timestamp).naive

    @classmethod
    def filter_by_date(cls, **kwargs):
        """Custom method to be able to filter by date,
        same reason as above, timestamp should be cast to naive Datetime."""
        if 'timestamp' in kwargs:
            kwargs.update(timestamp=arrow.get(kwargs['timestamp']).naive)

        return cls.query.filter_by(**kwargs)

    __tablename__ = 'metrics_event'

    id = Column(Integer, primary_key=True)
    work_uri = Column(String(255), nullable=False)
    value = Column(Integer, nullable=False)
    event_uri = Column(String(255), default='')
    country_uri = Column(String(22), default='')
    measure_uri = Column(String(255), nullable=False)
    timestamp = Column(DateTime, nullable=False)

    received_by_metrics_api = Column(Boolean, default=False)
    # 200/201 response when sent to the metrics API should be fine
    __table_args__ = (
        UniqueConstraint(
            'work_uri',
            'event_uri',
            'country_uri',
            'measure_uri',
            'timestamp',
            name='_unique_event'
        ),
    )

    def __str__(self):
        return f'<Event: {self.id} - {self.work_uri} - {self.measure_uri}>'

    def is_unique(self):
        query = self.query.filter_by(
            work_uri=self.work_uri,
            event_uri=self.event_uri or '',
            country_uri=self.country_uri or '',
            measure_uri=self.measure_uri,
            timestamp=self.timestamp,
        )
        return not queryset_exists(query)


class Scrape(Model):
    """Represents one driver/plugin running on one day for one service.

    The 'completed' field will be used to determine whether a scrape was
    successful or needs to be rerun.
    """

    __tablename__ = 'metrics_scrape'

    id = Column(Integer, primary_key=True)
    provider = Column(Integer, nullable=False)
    service_code = Column(String(255), nullable=False)
    datestamp = Column(Date, nullable=False)
    completed = Column(Boolean, default=False)

    __table_args__ = (
        UniqueConstraint(
            'provider',
            'service_code',
            'datestamp',
            name='_unique_scrape'
        ),
    )

    def __str__(self):
        plugin = get_enum_by_value(StaticProviders, self.provider).name

        return (
            f'<Scrape: {self.id} - {plugin} - {self.service_code} '
            f'-{self.datestamp}>'
        )
