import calendar
from datetime import datetime, timedelta
from itertools import chain
from pathlib import Path
from logging import getLogger

from core import db

import pycountry


logger = getLogger(__name__)

DIR = Path(__file__).parent
SPIDERS_FILE = f'{DIR}/spiders.txt'

SCHEMAS = {
    'doi': 'info:doi',
    'isbn': 'urn:isbn'
}

rename_mappings = {  # Country names in JStor mapped to their pycountry value.
    'Brunei': 'Brunei Darussalam',
    'Cape Verde': 'Cabo Verde',
    'Congo - Brazzaville': 'Congo',
    'Congo - Kinshasa': 'Congo, The Democratic Republic of the',
    "Côte d’Ivoire": "Côte d'Ivoire",
    'Falkland Islands': 'Falkland Islands (Malvinas)',
    'Hong Kong SAR China': 'Hong Kong',
    'Macao SAR China': 'Macao',
    'Micronesia': 'Micronesia, Federated States of',
    'Myanmar (Burma)': 'Myanmar',
    'Palestinian Territories': 'Palestine, State of',
    'Pitcairn Islands': 'Pitcairn',
    'Russia': 'Russian Federation',
    'São Tomé & Príncipe': 'Sao Tome and Principe',
    'Sint Maarten': 'Sint Maarten (Dutch part)',
    'St. Martin': 'Saint Martin (French part)',
    'St. Vincent & Grenadines': 'Saint Vincent and the Grenadines',
    'Vatican City': 'Holy See (Vatican City State)',
    'U.S. Virgin Islands': 'Virgin Islands, U.S.',
}


def get_spiders(spiders_file=SPIDERS_FILE):
    """Reads in known spiders and adds them to a set.

    Args:
        spiders_file (str): Path to spiders_file

    Returns:
        set: known spiders
    """
    with open(spiders_file, "r") as f:
        spiders = set(line.strip() for line in f)
        spiders.discard("")

    return spiders


def get_uri_schema_values(schema, uri):
    uri_scheme = SCHEMAS[schema]
    return uri_scheme, f'{uri_scheme}:{uri}'


def queryset_exists(queryset):
    """ Check if queryset exists in the database. Shorthand function to
    neaten up the code a bit.

    Args:
        queryset (flask_sqlalchemy.BaseQuery): result of running a filter

    Returns:
        bool: True if query exists otherwise False

    """
    return db.session.query(queryset.exists()).scalar()


def check_existing_entries(model_column, column_values):
    """ Get a tuple of values from a model, where entries with those values
    already exist in the database.

    Args:
        model_column (object): column to check in the form `Model.Column`.
        column_values (list): list of values to check - these
            should ideally be unique for the model.

    Returns:
        tuple: Values of entries that are already in the database.
    """
    return tuple(
        chain.from_iterable(
            db.session.query(
                model_column
            ).filter(
                model_column.in_(column_values)
            ).all()
        )
    )


# ## Start of common-driver logic:


def generate_dates(start_date, cutoff_date=None):
    """Yield dates, one day at a time from start_date until cutoff_date.

    Args:
        start_date (str): Starting date in YYYY-MM-DD format.
        cutoff_date(str): Optional end date in YYYY-MM-DD format.

    Yields:
        datetime: Days within the range specified, exclusive of cutoff date.
    """
    if not cutoff_date:
        cutoff_date = datetime.now().strftime('%Y-%m-%d')

    if not start_date:
        start_date = '2009-01-01'
    next_date = datetime.strptime(start_date, '%Y-%m-%d')
    cutoff = datetime.strptime(cutoff_date, '%Y-%m-%d')

    while next_date < cutoff:
        yield next_date
        next_date += timedelta(days=1)


def get_next_day(start_date):
    """Return current_date + 1 day.

    Args:
        start_date (str): Starting date in format YYYY-MM-DD.

    Returns:
        str: The next day in format YYYY-MM-DD.
    """
    start_dt = datetime.strptime(start_date, '%Y-%m-%d')
    end_dt = start_dt + timedelta(days=1)

    return end_dt.strftime('%Y-%m-%d')


def generate_queryset_chunks(full_queryset, set_size):
    """Generator function to loop through a large queryset in smaller chunks.

    Args:
        full_queryset (BaseQuery): Database query.
        set_size (int): number of entries per query subset.

    Yields:
        BaseQuery: Smaller subset of the original queryset.
    """
    step_size = 0
    query_subset = full_queryset.limit(set_size).offset(step_size)
    while query_subset.count():
        yield query_subset
        query_subset = full_queryset.limit(set_size).offset(step_size)
        step_size += set_size


def is_last_day_of_month(search_date):
    """Check if the search date passed is the last day of the month."""
    _date = datetime.strptime(search_date, '%Y-%m-%d')
    return calendar.monthrange(_date.year, _date.month)[1] == _date.day


def first_day_of_month(search_date):
    """Calculate the first day of the month for the given search_date."""
    _date = datetime.strptime(search_date, '%Y-%m-%d')
    first_day = _date.replace(day=1)
    return first_day.strftime('%Y-%m-%d')


def country_name_to_iso(country_name):
    """Convert a country name to its two-letter ISO 3166-2 code.

    Args:
        country_name (str): Full name of the country.

    Returns:
        str: Two-letter country code matching the ISO 3166-2 standard.
    """
    if not isinstance(country_name, str):
        return ''

    country_name = rename_mappings.get(
        country_name,
        country_name,
    ).replace('&', 'and').replace('St.', 'Saint')

    country = (
        pycountry.countries.get(name=country_name)
        or pycountry.countries.get(official_name=country_name)
        or pycountry.countries.get(common_name=country_name)
    )

    if not country:
        return ''

    return country.alpha_2
