"""Geo-lookup implementation"""

import csv
from ipaddress import ip_address
from logging import getLogger
from typing import List, Tuple, Union

import requests


logger = getLogger(__name__)

COUNTRY_RANGES = None  # Possibly replace with Caching


def get_country_ranges() -> list[tuple[int, int, str]]:
    geo_file_location = (
        'https://raw.githubusercontent.com/sapics/ip-location-db/main'
        '/geo-whois-asn-country/geo-whois-asn-country-ipv4-num.csv'
    )
    response = requests.get(geo_file_location, stream=True)
    return list(
        map(
            lambda x: (int(x[0]), int(x[1]), x[2]),
            csv.reader(response.iter_lines(decode_unicode=True), delimiter=','),
        )
    )


def find_value_in_range(
        value: int,
        range_list: List[Tuple[int, int, str]]
) -> Union[str, None]:
    """Return the element in range_list where value falls in the range.

        Args:
            value (int): Value to search for in the ranges.
            range_list (list): List of int ranges, and value for that range.

        Returns:
            tuple: Element in the range list given

    Assume each element in range list contains min, max value of range, as
    well as a value corresponding to that range. Values should be sorted, and
    must not overlap.
    """
    low, high = 0, len(range_list) - 1

    while low <= high:
        mid = (low + high) // 2
        range_min, range_max, code = range_list[mid]

        if range_min <= value <= range_max:
            return code
        elif value < range_min:
            high = mid - 1
        else:
            low = mid + 1

    return None  # value didn't fall into any range


def lookup_country_from_ip(ip_value: str) -> str:
    """Search for country code corresponding to IP Address given.

    Args:
        ip_value (str): IP4 Address

    Returns:
        str: 2-letter country code that matches the IP address searched.
    """
    global COUNTRY_RANGES

    if not COUNTRY_RANGES:  # Probably replace with Cache in the future.
        try:
            COUNTRY_RANGES = get_country_ranges()
        except ValueError:
            return ''

    ip_int = int(ip_address(ip_value))
    country_code = find_value_in_range(ip_int, COUNTRY_RANGES)

    return country_code or ''
