from celery.app.control import Control

from core import celery_app, create_app


app = create_app()


with app.app_context():

    app_control = Control(celery_app)
    app_inspect = app_control.inspect()

    if not app_inspect.active():
        raise Exception('No active workers found.')

    # It is possible for this to fail for other reasons
