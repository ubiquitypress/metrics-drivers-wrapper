"""
Script for testing purposes,
to be used for every plugin/driver
inside of the flask shell,
change the line 'StaticProviders.google_books'
for the desired driver/plugin to test.
This script won't save any data, it will print it.
"""
from core import plugins
from core.settings import StaticProviders

service_code = 'test'
# Change to run on different plugins
provider_enum = StaticProviders.google_books
search_date = '2023-07-25'

plugin = plugins.get_plugin(provider_enum).PROVIDER

events = plugin.process(
    search_date=search_date,
    service_code=service_code
)
for event in events:
    print(
        event.timestamp,
        event.measure_uri,
        event.country_uri,
        event.work_uri,
        event.value
    )
