from core import create_app, db
from processor.models import Event


from .variables import (
    event_1,
    event_2,
    event_3,
    event_1_wrong,
    event_2_wrong,
    event_3_wrong,
)


class SanitiseDois:
    def __init__(self):
        self.app = create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.events_set = [
            event_1,
            event_2,
            event_3,
            event_1_wrong,
            event_2_wrong,
            event_3_wrong,
        ]

    def create_events(self):
        """Create events in the database."""
        for entry in self.events_set:
            db.session.add(entry)
        db.session.commit()

    def process_events(self):
        """Retrieve all events and correct them."""
        all_events = Event.query.all()

        for event in all_events:
            self.correct_work_uri(event)

    def correct_work_uri(self, event):
        """Correct the work_uri field of the event."""
        if (
                event.work_uri.startswith("info:doi:10")
                and not event.work_uri.startswith("info:doi:10.")
        ):
            self.add_missing_dot(event)
        else:
            print("IGNORED: ", event.work_uri)

    def add_missing_dot(self, event):
        """Correct the DOI URI format by adding the missing dot."""
        prefix_len = len('info:doi:')
        event.work_uri = f'info:doi:10.{event.work_uri[prefix_len:]}'
        print(f"CORRECTED: {event.work_uri}")

