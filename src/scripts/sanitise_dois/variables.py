from processor.models import Event
from datetime import datetime


now = datetime.now()


event_1 = Event(
    work_uri='info:doi:10.13579/demo.001',
    value=3,
    event_uri='https://demo-site.com/value/1',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_2 = Event(
    work_uri='info:doi:10.13579/demo.002',
    value=3,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_3 = Event(
    work_uri='info:doi:10.13579/demo.0003',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

# ## wrong events
event_1_wrong = Event(
    work_uri='info:doi:1000156532',
    value=2,
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_2_wrong = Event(
    work_uri='info:doi:10576/12115',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_3_wrong = Event(
    work_uri='info:doi:10576/13484',
    value=2,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_4_wrong = Event(
    work_uri='info:doi:10576/12116',
    value=8,
    event_uri='https://more-demo-sites.com/value/3',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_5_wrong = Event(
    work_uri='info:doi:doi.org/10.35468/5869',
    value=11,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_6_wrong = Event(
    work_uri='info:doi:doi: 10.4324/9781003272120',
    value=11,
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_7_wrong = Event(
    work_uri='info:doi:http://dx.doi.org/10.14361/9783839433164',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)

event_8_wrong = Event(
    work_uri='info:doi:https://www.doi.org/10.3197/63801707455742.book',
    value=2,
    event_uri='https://another-demo-site.com/value/2',
    country_uri='urn:iso:std:3166:-2:DE',
    measure_uri='https://metrics.operas-eu.org/readership/demo-html/v1',
    timestamp=now,
)
