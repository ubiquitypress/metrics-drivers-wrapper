"""
Script for testing purposes,
to be used for every plugin/driver
inside of the flask shell,
change the line 'StaticProviders.google_books'
for the desired driver/plugin to test.
This script won't save any data, it will print it.
"""
from core import plugins # noqa
from core.settings import StaticProviders # noqa
from processor.models import Event, Scrape # noqa

service_code = 'test'
# Change to run on different plugins
provider_enum = StaticProviders.google_books
search_date = '2023-07-25'

# Create google_books scrape first
scrape_exists = Scrape.query.filter_by(
    provider=provider_enum
).exists().scalar()  # noqa
if not scrape_exists:
    _scrape = Scrape(
        datestamp='2022-01-01',
        completed=False,
        provider=provider_enum,
        service_code='test',
    )
    db.session.add(_scrape)  # noqa
    db.session.commit()  # noqa

scrape = Scrape.query.filter_by(provider=provider_enum).first()
plugin = plugins.get_plugin(scrape.provider).PROVIDER

events = plugin.process(
    search_date=search_date,
    service_code=service_code
)
for event in events:
    print(
        event.timestamp,
        event.measure_uri,
        event.country_uri,
        event.work_uri,
        event.value
    )
