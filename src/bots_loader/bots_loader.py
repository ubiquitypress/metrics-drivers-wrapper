from datetime import date
import json
from logging import getLogger
import re
from typing import List
import yaml

logger = getLogger(__name__)


class BotPatternsUpdater:
    """
    Update the bots file pattern to have the latest
    version available for the next search of bots using the user_agent.
    """

    def __init__(
        self, user_agents, robots_list, output_file, common_bot_words
    ) -> None:
        """
        Args:
            user_agents (str): Content from the spiders file.
            robots_list (list): List of Robots to update
            output_file (str): Path to the output file to update
            common_bot_words (list): List of the common bots
        """
        self.user_agents = user_agents
        self.robots_list = robots_list
        self.output_file = output_file
        self.common_bot_words = common_bot_words
        self.date_string = date.today().strftime("%Y-%m-%d")

    def generate_keywords_from_spiders(self) -> List:
        """Generate the bots keywords from the spiders user agents.

        Returns:
            List: Sorted list simplified from the spiders file
        """

        pattern_http = re.compile(r"https?://([^/ ]+)")
        keywords = self.sanitize_lines_spiders(self.user_agents)
        return sorted(
            self.simplify_keywords(
                keywords, self.common_bot_words, pattern_http
            )
        )

    def sanitize_lines_spiders(self, user_agents: str) -> List:
        """Get rid of the slashes created and repeated patterns.

        Args:
            user_agents (str): Lines of the spider file loaded

        Returns:
            List: Lines with no slashes
        """
        user_agents_list = [
            line.strip() for line in user_agents.split("\n")
        ]
        keywords_no_slash = [
            agent.replace("\\", "") for agent in user_agents_list
            if agent
        ]
        return list(set(keywords_no_slash))

    def simplify_keywords(
        self, keywords: List, common_bot_words: List, pattern_http: str
    ) -> List:
        """Search for keywords; 1st- Check with the most common ones.
        2nd- If the pattern is long, could mean it has got the URL of the bot,
        if not it's still worth getting the first 20 characters.
        3rd- Else the line is quite short so is worth saving it as it is.

        Args:
            keywords (List): Lines from the Spiders sanitized with no slashes
            common_bot_words (List): Most common bots keywords to search upon
            pattern_http(regex): https pattern to find the URLs

        Returns:
            List: List of bots simplified from the spiders file ready to save
        """

        list_bots = []
        for user_agent in keywords:
            # Check for common bot words (case-insensitive)
            if any(
                re.search(f"{re.escape(bot_word)}", user_agent.lower())
                for bot_word in common_bot_words
            ):
                for bot_word in common_bot_words:
                    for match in re.finditer(
                        rf"{re.escape(bot_word)}", user_agent.lower()
                    ):
                        list_bots.append(match.group())

            elif len(user_agent) > 25 and (
                matches := pattern_http.search(user_agent)
            ):
                url_words = matches.group(1).replace(")", "")
                list_bots.append(url_words)

            elif "Mozilla" not in user_agent:
                list_bots.append(user_agent[:20])
        return list(set(list_bots))

    def update_bot_patterns(self) -> None:
        """
        Update robots_list with new_bot_patterns loaded from the spiders
        if not already present.
        """
        for pattern in self.generate_keywords_from_spiders():
            if not any(
                pattern in item.get("pattern", "") for item in self.robots_list
            ):
                logger.info(f"Updating bot: {pattern}")
                self.robots_list.append(
                    {"pattern": pattern, "last_changed": self.date_string}
                )

    def save_bot_patterns(self) -> None:
        """
        Entry point of the class, Update the bots filtered and stripped
        and save robots_list to the updated counter robots file.
        """
        self.update_bot_patterns()
        with open(self.output_file, "w") as file:
            json.dump(self.robots_list, file)


def load_files():
    """
    Load the YAML config, user_agents spiders, and robots output file.
    """
    with open("yaml/demo-bots_loader.yaml", "r") as file:
        yaml_vars = yaml.safe_load(file)

    with open(f"{yaml_vars.get('files_dir')}spiders.txt", "r") as file:
        user_agents = file.read()

    with open(yaml_vars.get("output_file"), "r") as file:
        robots_list = json.load(file)

    return robots_list, yaml_vars, user_agents


def save_bots_patterns():
    """
    Load the needed files execute the update calling the
    BotPatternsUpdater class.
    """
    robots_list, yaml_vars, user_agents = load_files()

    bot_patterns = BotPatternsUpdater(
        user_agents,
        robots_list,
        yaml_vars.get("output_file"),
        yaml_vars.get("common_bot_words"),
    )
    bot_patterns.save_bot_patterns()


class BotsDetector:
    """
    If there is a robot_list created from the spiders load it,
    else load the actual bot_list file updated from the last time.
    """

    def __init__(self):
        self.tagged_as_bots = set()
        self.tagged_as_not_bots = set()
        self.robots_list, _, _ = load_files()

    def is_bot(self, user_agent: str):
        """Check if the given user_agent matches any pattern in robots_list.

        Args:
            user_agent (str): User agent sent by the access_logs

        Returns:
            bool: True if it's bot
        """

        if user_agent in self.tagged_as_not_bots:
            return False

        elif user_agent in self.tagged_as_bots or any(
            re.search(
                re.escape(item.get("pattern", "")), user_agent, re.IGNORECASE
            )
            for item in self.robots_list
        ):
            self.tagged_as_bots.add(user_agent)
            return True

        self.tagged_as_not_bots.add(user_agent)
        return False
