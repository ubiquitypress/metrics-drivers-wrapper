from logging import getLogger

import yaml

from flask import current_app

from core import consul
from processor.client import new_translator_client
from processor.logic import SCHEMAS
from processor.models import Event

from . import utils


logger = getLogger(__name__)


class DriverConfigException(Exception):
    """Exception that is specific to driver config being incorrect."""
    pass


class DriverNotReadyException(Exception):
    """Exception that signals that the driver is not ready to be run yet."""
    pass


class GenericDataProvider(object, metaclass=utils.MountPoint):
    """Plugins can inherit this mount point to add a data provider.

    Attributes:
        measure_uri (str): Name of measures collected from this plugin.
        provider (enum.IntEnum): Enum value used to identify this plugin or
            future plugins that replace this one.
        setting_slug (str): Name of Zookeeper setting where credentials for
            this plugin are stored.
    """

    def __init__(
            self,
            measure_uri,
            provider,
            setting_slug,
            api_base=None,
            client_class=None,
            validator=None
    ):
        self.measure_uri = measure_uri
        self.provider = provider
        self.setting_slug = setting_slug
        self.api_base = api_base
        self.validator = validator
        self.client_class = client_class

        if client_class and api_base:
            self.client = self.instantiate_client()

        self.uri_scheme = SCHEMAS['doi']
        self.uri_strict = True
        self.country_uri_scheme = 'urn:iso:std:3166:-2'
        self.translator = None

    def __str__(self):
        return self.__class__.__name__

    def init_app(self, app):
        """Init attributes that require app context."""
        with app.app_context():
            self.translator = new_translator_client()

    def instantiate_client(self):
        return self.client_class(self.api_base)

    def _add_validator_context(self, **kwargs):
        """ Add kwargs as additional context to the Marshmallow validator.

        These will generally be independent of information retrieved from an
        API call - e.g. URI ID, Scrape ID, etc.
        """
        self.validator.context = kwargs

    def standardise_country(self, country_code):
        if country_code:
            return f'{self.country_uri_scheme}:{country_code}'
        return ''

    @staticmethod
    def get_event(uri_id, subject_id):
        """Tries to get an event to prevent duplicates from being created. """
        return Event.query.filter_by(
            uri_id=uri_id,
            subject_id=subject_id
        ).first()  # like step 1 of get_or_create()

    def fetch_credentials(self, service_code, enforce_active=True):
        """Fetch credentials required by plugin from zookeeper.

        Args:
            service_code (str): Service code of metrics service.
            enforce_active (bool): Return result even if setting is not active.

        Returns:
            dict: Credentials specific to the plugin.
        """

        settings_source = current_app.config.get('DRIVERS_SETTINGS_SOURCE')

        if settings_source == 'YAML':
            settings, is_active = self.load_yaml_settings(service_code)
        elif settings_source == 'CONSUL':
            settings, is_active = consul.get_settings(
                service_code=service_code,
                setting_type=self.setting_slug,
            )

        else:
            raise Exception("settings_source value not specified")

        if not is_active and enforce_active:
            raise DriverConfigException(
                f"'{self.setting_slug}' credentials are unavailable for "
                f"service '{service_code}'."
            )

        return settings

    def load_yaml_settings(self, service_code):
        """Load yaml file from the 'yaml' folder, by service code
           and plugin type. Test yaml files are included in the
           'yaml' folder for reference.

        Args:
            service_code (str): Service code of metrics service.
            plugin (str): plugin type, from the function that calls
            fetch_credentials.

        Returns:
            dict: Credentials specific to the plugin.
            boolean: is_active flag.
        """
        with open(f'yaml/{service_code}-{self.setting_slug}.yaml', 'r') as f:
            settings = yaml.safe_load(f)
            is_active = settings.pop('active', None)

        return settings, is_active

    def set_uri_schemes(self, credentials):
        """Set multiple URI schemes for the plugin.

        Args:
            credentials (dict): Configuration for running the driver.

        Returns:
            list: URI schemes to check against when saving metrics.
        """
        uri_scheme_list = credentials.pop('uri_scheme', self.uri_scheme)

        if isinstance(uri_scheme_list, str):
            uri_scheme_list = [uri_scheme_list]
        elif not isinstance(uri_scheme_list, list):
            raise ValueError('URI Scheme must be str or list')

        return uri_scheme_list

    def multi_scheme_uri_to_id(
            self,
            uri: str,
            uri_schemes: list,
            uri_strict: bool,
    ) -> list:
        """perform translator.uri_to_id() with multiple URI schemes.

        Args:
            uri: URI to translate.
            uri_schemes: URI schemes to run against the translator.
            uri_strict: Output errors with ambiguous translation queries.

        Returns:
            list: URIs matching the schema specified.
        """
        for uri_scheme in uri_schemes:
            identifiers = self.translator.uri_to_id(
                uri,
                uri_scheme,
                uri_strict=uri_strict,
            )
            if identifiers:
                return identifiers

        return []
