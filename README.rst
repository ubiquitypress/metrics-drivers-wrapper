Metrics Service
===============

Service to run metrics drivers, store the output in a database, and send it to
the Metrics API.


How to run the service
----------------------


Set up a virtual environment
............................

.. code-block:: bash

    $ mkvirtualenv metrics-service --python=`which python3.12.1`

In this venv, install the project requirements.

.. code-block:: bash

    $ pip install -r requirements.txt


Set up a database
.................

The Metrics service has been written to use PostgreSQL. You will need to
create a database instance, as well as a user who can access this database
before continuing.

.. code-block:: bash

    $ sudo -u postgres createdb metrics_db
    $ sudo -u postgres psql -c "CREATE USER metrics_user WITH PASSWORD 'metrics_password';"
    $ sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE metrics_db TO metrics_user;"

Create a file:
.. code-block:: bash

    $ nano ~/.bash_aliases
    $ source ~/.bash_aliases

With the below content:
.. code-block:: bash

    # ==========================
    # METRICS-DRIVERS-DB-WRAPPER
    # ==========================

    alias go2metrics-service="cd <METRICS-DRIVERS-WRAPPER-HOME>/src/; pyenv activate metrics-drivers"

    alias with_md_env_="~/.bash_scripts/load_metrics_drivers_env.bash"
    alias with_md_env_test="~/.bash_scripts/load_metrics_drivers_env_test.bash"

    alias flask_metrics_="with_md_env_ flask"
    alias flask_metrics_shell="with_md_env_ flask shell"
    alias flask_metrics_run_tests="with_md_env_test python -m unittest discover core.tests -t . -v"


Create a directory and a bash file inside of it:
.. code-block:: bash

    $ mkdir ~/.bash_scripts/ && cd ~/.bash_scripts/
    $ nano load_metrics_drivers_env.bash

Add the following:
.. code-block:: bash
    #!/bin/bash
    export DRIVERS_SETTINGS_SOURCE='YAML' # Change for production
    export FLASK_APP=core
    export FLASK_ENV=development
    export CONFIG=DevConfig

    export RMQ_HOST=localhost
    export RMQ_VHOST=metrics
    export RMQ_PASSWORD=password
    export RMQ_USER=user

    export DB_USER='metrics_user'
    export DB_PASSWORD='metrics_password'
    export DB_HOST='localhost'
    export PORT='5432'
    export DB_NAME='metrics_db'

    # Live Values - be careful
    export TOKENS_KEY=your-token
    export TRANSLATION_API_BASE=https://translation-sample.press

    # Live Values - be careful
    export CONSUL_HOST=consul.your-instance
    export CONSUL_TOKEN=your-token

    ARGS=("$*")

    $ARGS

Give it permissions:
.. code-block:: bash
    $ chmod u+x ~/.bash_scripts/load_metrics_drivers_env.bash

Back to the repository, run:
.. code-block:: bash
    $ flask_metrics_ db upgrade

If getting error:
.. code-block:: bash
    sqlalchemy.exc.ProgrammingError: (psycopg.errors.InsufficientPrivilege) permission denied for schema public
    LINE 2: CREATE TABLE alembic_version (
                        ^
    [SQL: 
    CREATE TABLE alembic_version (
            version_num VARCHAR(32) NOT NULL, 
            CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num)
    )

]

Try the command:
.. code-block:: bash
    $ sudo -u postgres psql -c "GRANT postgres TO metrics_user;"

Make sure the flask shell works:
.. code-block:: bash
    $ flask_metrics_shell

Finally, using the shell, test the desired driver/plugin using the script located in:
`src/scripts/test_script_drivers.py`

- Desired output:
    .. code-block:: bash
        2024-02-14 00:00:00 https://downloads.test.com urn:iso:std:1234:-1:US info:doi:10.1234/abc 1
        2024-02-14 00:00:00 https://downloads.test.com urn:iso:std:5678:-1:US info:doi:10.1234/def 2
        ...

Setup the crossref_citedby_driver
................................
.. code-block:: bash

    $ python setup.py build
    $ python setup.py install


Using Docker
------------

First create a env.docker file with the following env variables, please,
bear in mind the values for live environment and Linux/MacOs values:

.. code-block:: bash
    #Set up for local settings files purposes, when not using CONSUL!
    DRIVERS_SETTINGS_SOURCE=YAML
    #
    # CONSUL, for production purposes!
    CONSUL_HOST=consul.your-instance
    CONSUL_TOKEN=your-token
    #  
    FLASK_APP=core
    FLASK_ENV=development
    CONFIG=DevConfig
    # RMQ Setup
    RMQ_HOST=localhost
    RMQ_VHOST=metrics
    RMQ_PASSWORD=password
    RMQ_USER=user
    RMQ_AMQ_SCHEME=amqps
    RMQ_PORT=5671
    # DB setup  
    DB_USER=metrics_user
    DB_PASSWORD=metrics_password
    #
    # For macOs!
    DB_HOST=docker.for.mac.host.internal
    #
    # For Linux!
    DB_HOST=127.0.0.1
    #
    PORT=5432
    DB_NAME=metrics_db
    TOKENS_KEY=password
    TRANSLATION_API_BASE=https://sample-translator.com

Then run:

.. code-block:: bash

	$ sudo docker build -t metrics-drivers -f docker/Dockerfile .

MacOS:

.. code-block:: bash

    $ docker run --add-host=host.docker.internal:host-gateway --env-file env.docker metrics-drivers:latest

Linux:

.. code-block:: bash

	$ docker run --network="host" --env-file env.docker metrics-drivers:latest

And finally, in a separated shell:

.. code-block:: bash

	$ docker exec -ti <container-id> bash

Once inside the container, check the flask shell works:

.. code-block:: bash

    $ flask shell

Changelogs
----------

For Changelogs, please divide any changes into these categories:

 * Added
 * Fixed
 * Changed
 * Removed


Credits
-------

* OPERAS / HIRMEOS for having funded, supported and advised the development
* Marty Alchin and Régis Décamps for the `KISS plugin architecture`_


.. _KISS plugin architecture: https://github.com/regisd/simple_plugin_framework
