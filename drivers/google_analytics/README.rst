Hirmeos google-analytics-driver Python library
==============================================

Functions required to run the HIRMEOS google_analytics_driver package developed for the
OPERAS HIRMEOS project.


Starting point:
https://github.com/hirmeos/google_analytics_driver/tree/e64dd4b33e3c704d3d181db85b2f156a89ae51bf


Code for the HIRMEOS project is avaiable at https://github.com/hirmeos

For more information about the OPERAS metrics, go to
https://metrics.operas-eu.org/


-----

Note: We are still in the early stages of this package so there will likely be
many breaking changes as we go along.

Release Notes:
==============

[0.1.1] - 2023-07-28
---------------------
Changed
.......
 - Update using a pyproject.toml file as the new deployment structure instead of the setup.py. 

[0.0.3] - 2020-03-06
---------------------
Changed
.......
 - Updated requirements to be more flexible (avoid conflicts with other
   packages).
