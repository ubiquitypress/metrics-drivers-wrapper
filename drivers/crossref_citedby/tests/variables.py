from collections import namedtuple
import os


def mock_json():
    return dict(
        message={
          "published-print": {"date-parts": [[2022, 9]]},
          "created": {
            "date-parts": [[2022, 5, 19]],
            "date-time": "2022-05-19T11:21:25Z",
            "timestamp": 1652959285000
          },
          "published-online": {"date-parts": [[2022, 5, 19]]},
          "deposited": {
            "date-parts": [[2022, 9, 19]],
            "date-time": "2022-09-19T12:13:35Z",
            "timestamp": 1663589615000
          },
          "published": {"date-parts": [[2022, 1, 1]]}
        }
    )


MockResponse = namedtuple('MockResponse', ['status_code', 'json'])
mock_response = MockResponse(
    status_code=200,
    json=mock_json,
)

xml_filepath = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    'CrossrefCitations_2023-05-25.xml'
)

test_xml = """
    <journal_cite fl_count="0">
    <issn type="print">3456789</issn>
    <journal_title>Journal Test</journal_title>
    <journal_abbreviation>Journal Test</journal_abbreviation>
    <article_title>Quality assurance and safety Test</article_title>
    <contributors>
    <contributor contributor_role="author" first-author="true" sequence="first">
    <given_name>Test Name</given_name>
    <surname>Test Surname</surname>
    </contributor>
    <contributor contributor_role="author" first-author="false" sequence="additional">
    <given_name>Test Name 2</given_name>
    <surname>Test Surname 2</surname>
    </contributor>
    <year>2023</year>
    <publication_type>full_text</publication_type>
    <doi type="journal_article">12.3456789/test.12345</doi>
    </contributors>
    </journal_cite>
"""
