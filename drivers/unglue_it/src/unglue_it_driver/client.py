"""Currently unused client - offers an alternative means of interacting with
the Unglue.it API.
"""

from dataclasses import dataclass
from typing import Any

import requests

from logging import getLogger
from typing import Dict, Iterator

from .serializers import UnglueReportItem

logger = getLogger(__name__)


@dataclass
class UnglueitClient:

    user: str
    api_key: str
    base_url: str = "https://unglue.it"

    def __post_init__(self):
        self.params = dict(
            format="json",
            api_key=self.api_key,
            username=self.user,
        )

    def _get(self, path: str) -> Any:
        """Retrieve results based on the path.

        Args:
            path (str): Path for the request

        Returns:
            Any: JSON response from the get request
        """
        url = f'{self.base_url}/{path}'
        response = requests.get(url, params=self.params)

        if response.status_code != 200:
            response.raise_for_status()

        return response.json()

    def get_results(self, ebook_path: str) -> Dict:
        """Helper method for the fetch_results method.

        Args:
            ebook_path (str): path for the api referencing the book

        Returns:
            Dict: Results isbn + downloads
        """
        book_results = self._get(ebook_path.lstrip("/"))
        if identifiers := self._get(book_results["edition"]):
            for identifier in identifiers.get("identifiers", []):
                if identifier.get("type") == "isbn":
                    book_results["isbn"] = identifier.get("value")
                    return book_results

    def fetch_results(self, publisher: int) -> Iterator[UnglueReportItem]:
        """Fetch book report items for a given publisher.

        Args:
            publisher (int): ID of publisher to search.

        Returns:
            Iterator: Result from the Serializers
        """
        publisher_and_path = self._get(f"api/v1/publisher/{publisher}")
        for ebook_path in publisher_and_path["ebooks"]:
            if book_results := self.get_results(ebook_path):
                yield UnglueReportItem(**book_results)
